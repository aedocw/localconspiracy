contact:
	name: "Christopher Aedo"
	email: "doc@aedo.net"

intro: >
	As an approachable geek I am as comfortable talking tech with a
	stranger in an airport as I am with my closest friends.  Few
	things bring me more pleasure than digging deep into new
	technology in my spare time, building something around it, and
	then talking about what I learned and how much fun I had doing
	it.  I'm also famous amongst my friends for engaging random
	people in conversation around what they do, how their business
	works, and what technologies could be of tremendous benefit to
	them.  To put it simply, I love nothing more than understanding
	how something works, and how I might be able to help improve it.

work:
    -
	employer: IBM, Armonk, New York
	start: August 2015
	end: present
	description: >
	IBM has been a leading innovator for over 100 years.  Inventions
	by IBM include the automated teller machine (ATM), the PC, the
	floppy disk, the hard disk drive, the magnetic stripe card, the
	relational database, the SQL programming language, the UPC
	barcode, and dynamic random-access memory (DRAM).  They also
	created the Selectric typewriter series, which were some of the
	finest typing machines ever made.

	-
		role: Developer Advocacy Program Manager
		start: November 2016
		end:  present
		description: >
		Managing a team of open source developers with the mission to
		transform IBMs image in the global developer community by being
		emissaries for the most exciting technologies and by guiding
		internal teams to ensure IBM's product offerings are relevant to
		the widest possible audience.
	-
		role: OpenStack Innovation Team
		start: August 2015
		end: November 2016
		description: >
		Our small team implemented a distributed, easily scalable cloud
		based on OpenStack deployed continuously from upstream master.
		Using Gerrit for code review and automating sync of local changes
		with upstream commits we were able to increase contribution and
		minimize impacts normally associated with deploying only stable
		releases.  Development velocity of 200 globally distributed
		engineers significantly increased through use of the platform we
		built and trained them on.
	-
		accomplishments:
		- Working with an SVP and four others, create new developer advocacy organization which will ultimately be staffed up to at least 250 advocates around the world
		- Wrote document which was used as cornerstone of our Chief Developer Advocates “Developer First” approach for entire advocacy organization
		- Coordinated with marketing and events teams to support effective sponsorship and marketing around IBM’s developer focused initiatives
		- Increased IBM’s presence and relevance in developer communities where IBM previously had a small footprint by securing sponsorship and helping with organization of multiple conferences (such as DevOps Days, OS Bridge, and others)
		- Organized multiple advocacy road-show initiatives, coordinating with local meetup and user group organizers around the world
		- Collected developer experience feedback from users during workshops and at conferences, using that feedback to work with internal offering managers on improving products to increase retention and satisfaction
		- Numerous speaking engagements at conferences including delivering a keynote at OSCON 2017
		- Oversaw esports work with Watson to bring predictive analytics to major tournaments and provide tools for team managers to analyze performance in StarCraft II, Dota 2 and Counter-Strike: Global Offensive
		- Created training program for using Gerrit and developing software in open source communities; delivered to a dozen different global teams; new code review practices resulted in improved quality of code, fewer bugs and better coordination and communication

    -
	employer: Mirantis, Mountain View, California
	start: March 2014
	end: July 2015
	description: >
	Mirantis delivers the technology, integration, training and
	support required for companies to succeed with production-grade
	open source cloud. More customers rely on Mirantis than any other
	company to scale out OpenStack without the compromises of vendor
	lock-in. Mirantis’ bench of 400+ open source infrastructure
	experts helped make them one of top 5 contributors to OpenStack’s
	upstream codebase.

	-
	role: Product Architect
	description: >
	Working with the product management team, multiple engineering
	teams, Mirantis’ customers and the community of OpenStack
	contributors providing strategic internal leadership in order to
	ensure our near and long term product goals are aligned with both
	market needs and where the code and community are headed.
	Perform analysis and research of competitive and complementary
	products and technologies for the product, sales and marketing
	teams as well as performing technical partner analysis and
	evaluation.  Work closely with the documentation and training
	teams to increase the user and contributor base.  In addition to
	being an active OpenStack contributor, organized the Portland
	OpenStack User Group to strengthen the local developer community.

	-
	accomplishments:
		- Coordinated the build and launch of the OpenStack Community App Catalog, available at http://apps.openstack.org, and serving as the Project Technical Lead.
		- Launched OpenStack PDX User Group in Portland, and maintaining a six month lead on scheduled topics and speakers
		- Developed and taught cloud computing curriculum for Oregon BioSciences Association (biocatalyst.org)
		- Improve community interaction by acting as liaison between OpenStack community and internal groups; highlight relevant content of daily OpenStack mailing list & IRC traffic to internal groups, ensure internal experts maintain active community presence
		- Lead Fuel documentation restructure for the sake of improving community interaction, including significant reorganization of multiple disparate documentation elements and rewriting and reorganizing the OpenStack Fuel wiki page
		- Presented at conferences and meetups in Los Angeles, Portland, New York, Tokyo, Okinawa as well as presenting and sitting on panels at several OpenStack design summits
		- Working with the NY Financial Services Open Cloud Forum to foster collaboration between the world’s leading banks around the transition to cloud and related regulatory planning
		- Frequently provide content and consultation for Marketing and Sales groups
		- Led several webinars including What's New in Kilo and What's New in Juno
		- Ongoing competitive analysis, focused around the OpenStack deployment landscape (Fuel vs. Helion, Ubuntu/Juju, RHEL OSP, Rackspace/os-ansible)
		- Regularly serve as last minute technical expert for sales team on calls and customer visits
		- Evaluate and coordinate containerization efforts and advancements (Docker, Rocket and LXD), as well as related efforts to use containers for the OpenStack control plane (internally championing Kolla, Bi-Frost and Ansible as a foundation for future deployment tooling)

### Morphlabs, Manhattan Beach, California (August 2011 to January 2014)
Recognized industry leader in complete cloud solutions for the
Service Provider and Enterprise. Offering an integrated
hardware/software platform combining best-of-breed software and
optimized + certified hardware to deliver an extremely efficient
open-source cloud.  Founded in 2007 by industry pioneers,
Morphlabs, Inc. is headquartered in Los Angeles, with operations
in the Philippines, Japan, and Singapore.

#### Chief Technology Officer
Provide technology leadership to sales, engineering and delivery
teams.  Interface with customers to design solutions that best
fit their needs.  Represent the product owners perspective, lead
internal engineering team to continually improve our product by
anticipating and planning for industry needs.  Frequently speak
at technology conferences and meetups locally and internationally
as an evangelist not just for mCloud but also for cloud
computing, software defined networking, software defined storage
and systems automation.

	accomplishments:
		- Negotiated strategic partnerships and solution certification agreements with Dell, NEC, Hitachi, Media Temple
		- Restructured technology arm of company while leading strategic transition from Eucalyptus to OpenStack
		- Implemented agile development and guided team to fully automated Puppet-based deployments and updates, and CI with Jenkins
		- Developed 24/7 customer support product, built support team with globally distributed members to provide support services to global and domestic telecom companies and service providers
		- Directed creation of optimally balanced price/performance OpenStack-based cloud platform including SDN and Ceph-based software defined storage
		- Drove feature innovations directed towards capture of enterprise market as well as service providers
		- Created StackHack OpenStack IaaS contest and launched in Asia to establish our position as regional leaders in cloud expertise while providing cloud usage workflow development/optimization education to nascent market
		- Evaluated technical partnerships for potential adoption (Nexenta, Arista, Astute Networks, Stackato and many others)
		- Created and mentored documentation team charged with creation of print-ready customer documentation as well as internal-only development and deployment manuals
		- Maintained effective leadership while managing teams located in multiple US cities, the Philippines, Singapore and Tokyo
		- Successfully coordinated and passed SecureWorks Penetration Test/Security analysis
		- Active in local DevOps, OpenStack, Ruby and Unix communities
		- Frequently present or participate in panels at local meetups as well as multiple OpenStack Summits, forums in the Philippines and Japan, Flash Memory Summit, CloudOpen, PuppetConf and Unix Users Association of Southern California
		- As holder of fixed-wing pilots license, frequently fly myself and team-members to meetings in central and southern California at significant savings and dramatically increased convenience

### thrdPlace, Long Beach, California (August 2013 to January 2014)
thrdPlace is a crowdsourcing and management application for
community service and development. It is the first web and mobile
platform to enable community members to collaborate based on
their interest, ability and activity in the built environment.
Founded to address a commonly occurring problem in all
communities: individuals and organizations alike, fail to capture
the potential energy within their own communities. thrdPlace
brings together motivated individuals with organizations that can
supply the financial and physical means to accomplish positive
change.

#### Chief Technology Officer
Lead systems and engineering team in building a highly scalable
platform. Coordinate integration points with technology partners.
When negotiation with platform partners, articulate the
advantages to their technology teams while assessing the best
approach for them to utilize the platform seamlessly.

### Xen, Los Angeles, California (February 2010 to August 2011)
Well-funded technology startup with over 20 employees working in the social space focused on identity management, interest identification and analytics. (No relation to Xen Hypervisor virtualization technology).

#### Chief Information Officer
Provide information technology leadership, vision and direction
both internally and for clients and partners. Define and
implement most cost effective approach to provide core technical
infrastructure to maximize business productivity and remove
communication, development and execution barriers.

### Schematic, Los Angeles, California (2002 to February 2010)
Global interactive services and design agency headquartered in
Los Angeles with offices in San Francisco, New York, Atlanta,
Austin, Minneapolis, London and Costa Rica.

#### Vice President, Technology Operations
Oversee corporate information technology, provide leadership and
strategic direction, oversee facilities management group,
maintain SOX compliance, launch revenue-generating Internet
hosting center, and implement cost effective changes/initiatives
to improve efficiency, effectiveness and profitability of
business operations.

### Vivid Entertainment Group, Los Angeles, California (1999-2002)
Industry-leading entertainment company pioneering Internet-based
video-on-demand delivery.

#### Vice President, Internet Operations
Increased profit by transitioning Internet operations away from
outside company and creating a new Internet division within Vivid
Entertainment, helping to grow that division from $200k to over
$30m/year in revenue.

### Andersen Worldwide, Chicago, Illinois
Parent company of Arthur Andersen LLP, a leading public
accounting firm, and Andersen Consulting, now known as Accenture.

#### Network Administrator/EDP Auditor (1992-1996)
#### Webmaster/Systems Administrator (1996-1999)

### Education
University of Illinois at Chicago (1990-1994),
Mathematics/Computer Science & Philosophy

