Title: Reverse SSH tunnel with SOCKS proxy
Date: 2020-08-19 17:57
Author: Christopher Aedo
Tags: 
Slug: reverse-tunnel

![image](/images/tunnel-diagram.png "network diagram showiing workstation connection to bastion host, then a firewall and a secret VM behind the firewall")

(I'm writing this mostly for myself so if/when some day in the future I
want to set this up again and can't remember how, I've got something
to reference.)

If you have a scenario where you'd like to access machines behind a
corporate firewall without getting on their VPN, this might work for
you. For instance if you occasionally need to access things behind the
firewall from a machine running an OS (like linux) that is not
supported by the IT overlords at your $JOB, you can set up a reverse
SSH tunnel to connect a machine behind the firewall to a machine at
home (or a VM on a cloud provider).

First off, get a VM up and running on the office network, install
autossh, copy your SSH id to your bastion host, and then start a
reverse tunnel. This would be done on "SECRETVM".

~~~~
$ sudo apt install autossh
$ ssh-copy-id bastion
$ autossh -M 10984 -o "PubkeyAuthentication=yes" -o "PasswordAuthentication=no" -i /home/username/.ssh/id_rsa -R 2224:localhost:22 username@bastion -p 22
~~~~

Now on your home machine (in the diagram above thats "Workstation") you
need to add an entry to ~/.ssh/config

~~~~
host secretvm
   User username
   ProxyCommand ssh bastion -W localhost:2224
~~~~

Now, to get to the VM inside the office, you can just "ssh secretvm"!

To proxy your web traffic through that VM (so you can reach things
like JIRA easily), use a SOCKS proxy. Run the following:

~~~~
$ ssh secretvm -D 9932 -N
~~~~

Then in Firefox, go to Preferences, General, Network Settings and
select "Manual proxy configuration", set SOCKS Host to localhost with
port 9932, and check the box for "Proxy DNS when using SOCKS v5".

![image](/images/socks-config.png "sample SOCKS configuration screen")

Enjoy! Also don't tell IT as this could be a real security problem if
your bastion host is not well secured. So be sure to do everything you
can to lock that node down and keep it up to date.

EDIT: Adding a link to this excellent [visual guide to SSH tunnels](https://robotmoon.com/ssh-tunnels/)
as it's SUPER useful!

EDIT: Adding a link to a MUCH faster SOCKS-over-SSH method, [Rapid SSH
Proxy](https://github.com/Snawoot/rsp)
