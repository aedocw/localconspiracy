Title: Peanut Butter Banana Pancakes
Date: 2018-01-07 14:31
Author: Christopher Aedo
Tags:  pancakes, cooking, food
Slug: pbpancakes

Over the last year I've been making small tweaks to my [Aedo Family
Secret Pancake Recipe](/2011/08/pancakes.html), and I think I've
finally perfected it.  Rather than update the original post I thought
it would be better to make a new one for reference.  Ultimately the
modifications were pretty small (less olive oil, almond milk
substitution, peanut butter and a process change). Adding in the
powdered peanut butter gives it a little more protein and it's just
enough to give it a little bit of peanut butter and banana taste. The
end result though is pretty damn great!

Regarding that process change - the recipe uses 1.5c dry oatmeal.
I've started blending that into a powder before adding any other
ingredients.  It makes a nice difference in terms of smoothing the
texture and keeping it consistent from batch to batch.

> UPDATED Aedo Family Secret Pancake Recipe:  
> 1 and a 1/2 cup dry quick-cook oatmeal (blended to flour consistency)  
> 1/2 cup almond milk  
> 1 cup cottage cheese  
> 6 eggs  
> 1/4c PBFit powdered peanut butter  
> 1/2 tsp salt  
> 2 tbsp olive oil  
> 2 bananas  

1. Place oats in blender and process until powdered
2. Add remaining ingredients 
3. Blend until smooth consistency is reached
4. Pour 4" cakes on griddle at medium-low heat

Nutritional information ([from MyFitnessPal.com](https://myfitnesspal.com)):

![PBPancaks Nutrition](/images/pbpancakes-nutri.png)
