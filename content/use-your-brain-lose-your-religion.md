Title: Use your brain; lose your religion
Date: 2012-04-27 15:30
Author: Christopher Aedo
Tags:  downfall of humanity, beating a dead horse, improvement, religion
Slug: use-your-brain-lose-your-religion

Those who know me well will not be surprised to see me share a few
articles on this topic.  I've said for years that the most dangerous
thing about leading a "faith based" life is that it dulls critical
thinking.  In order to live that way, you need to actively train your
brain to NOT think about some things.  These two articles reference a
related study, finding that analytic thinking leads to a a loss of faith
(who would be surprised by that?)  

> "Analytic thinking undermines belief because, as cognitive
> psychologists have shown, it can override intuition. And we know from
> past research that religious beliefs—such as the idea that objects and
> events don't simply exist but have a purpose—are rooted in intuition.
> "Analytic processing inhibits these intuitions, which in turn
> discourages religious belief," Norenzayan explains." ([from Scientific
> American](http://www.scientificamerican.com/article.cfm?id=losing-your-religion-analytic-thinking-can-undermine-belief))

> "Charles Darwin and Albert Einstein famously did not believe in a
> supernatural God, and neither do some scientists today. It now appears
> there may be a good reason for this: thinking analytically dims
> supernatural beliefs, apparently by opposing the intuitive thought
> processes that underpin them." ([from New
> Scientist](http://www.newscientist.com/article/dn21749-analytical-thinking-erodes-belief-in-god.html))

In a very related series "[Confessions of an
Ex-Priest](http://francishunt.blogspot.de/2011/04/confessions-of-ex-priest-1.html)"
and "[Dismantling the
Vatican](http://francishunt.blogspot.com/2012/04/dismantling-vatican-ii.html)", a
man who spent eight years being trained for his position only to leave
the church after a single year as an ordained priest reflects on
religion and the power structure the church relies on.  In part 2 he
talks about an exciting moment in the catholic church, when a feeling of
openness and empowerment almost swept through the organization.  Luckily
the bishops and priests managed to shut it down, lest their followers be
encouraged to actually read the book their faith is based on.  

> "That the Catholic Church, or, rather, those who lead it and exercise
> power within it, prefer a message dominated by pessimism, fear, and
> control, rather than one inspired by openness, trust, and hope seems
> to me to show a weakness of faith in the life and message of the
> man/God they claim as their foundation and inspiration."

  

</p>

