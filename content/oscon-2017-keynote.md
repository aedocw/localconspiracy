Title: Delivering a keynote at OSCON 2017
Date: 2017-05-27 14:30
Author: Christopher Aedo
Tags: speaking, improvement
Slug: oscon-2017-keynote


Working as a developer advocacy program director for IBM,
improving how the world of developers perceive our company is my
top priority. One of the ways I believe we can make a huge impact
is by using our time in front of developers wisely.  That means
no product pitches at tech conferences; out of respect for the
time of attendees, we need to talk about things that matter.

![on stage at OSCON](/images/on-stage-oscon3.png)

At this years [OSCON 2017 in Austin,
TX](https://conferences.oreilly.com/oscon-tx) I had a pretty
great opportunity to give a talk and do something consistent with
these ideals.  I got to deliver [a 10 minute
keynote](https://www.youtube.com/watch?v=uG2_6rjL_KI) on the
importance of choosing open platforms when building open source
software (#OpenInfra).

Though I'd attended several OSCON's in the past, this was my
first time taking the big stage.  The whole experience was a ton
of fun, and I hope I have a chance to do something like that
again.  Everyone involved with the planning and production was
super professional and buttoned up.  Getting ready backstage felt
kind of like being behind the scenes at a really polished rock
concert.  I am not sure the picture I took (below) captures it
well enough but there was a lot going on, but everyone was so
calm and relaxed you would never have known it.

![OSCON backstage](/images/oscon-backstage.jpg)

The slides and abstract of the talk are [available
here](https://conferences.oreilly.com/oscon/oscon-tx/public/schedule/detail/61296).
I used baroque paintings so it would be easy to keep the slides
consistent and also not sweat any copyright violations. Looking
through all that art was a lot of fun.  There were a great many
really weird paintings that made me wonder who the hell stood
still long enough for the artist to capture that!  One of my
slides used this amazing painting of two kids posing for their
portrait, but the boy has a kitten over on arm and is holding an
eel in his other hand.  What the hell?

![gitlab, gitea and gogs](/images/choose-gitlab.png)

The talk (about open infrastructure) was meant to draw attention
to the underlying infrastructure we use when building open
software collaboratively.  Not just the "I" in IaaS (compute,
storage and networking), but the other tools, platforms and
technologies we rely on.  Things like repository management
platforms, asynchronous and synchronous communications, testing
systems, etc.  These are all things we tend to forget about, or
at least not think about them as core parts of open source
projects.  There's a lot of risk though to not choosing
carefully.  The heart of the talk was about making platform and
technology choices based on three open principles: Transparency,
Interoperability and Influence.

![open principles](/images/open-principles.png)

Transparency: does the platform or project you are using operate
in a transparent manner?  Do you have a clear sense of what
they're planning, and if there's a company behind the effort how
healthy is it?  [Gitlab](https://www.gitlab.com) is a great
example of how to do this right.  They're a privately run
startup, but they operate completely in the open.

Interoperability: if you choose a platform, what's the risk of
being locked in?  Can you take your data elsewhere if you decide
to make a different choice?  Are they based on (and even
contributing to) open standards?  I used the example of IRC in my
talk, as it's hard to choose a more open and interoperable
standard for team communications.

Influence: can we (open source community, generally non-paying
users) influence them?  Do they have an open and easy way to
report bugs or request features?  Do they accept code
contributions from outsiders?  How well do they listen to their
users, especially the non-paying ones? [Travis
CI](https://travis-ci.com/) is a really great example here - they
go out of their way to work with the open source communities
using their testing infrastructure to make sure they service they
are offering for free is working well for them.

The feedback I got in person at the conference was amazingly
positive.  It was a message that resonated with a lot of
attendees.  A bunch of people also took pictures and quoted the
talk in tweets, which was really gratifying.

I thoroughly enjoyed the experience, and I'm glad a lot of folks
were on the same page as me on this topic.  Hopefully I'll get to
talk about something equally important to the community next year
when OSCON returns to Portland!
