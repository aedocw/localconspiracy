Title: No Innocent Bystanders
Date: 2020-07-07 08:15
Author: Christopher Aedo
Tags: 
Slug: no-innocent-bystanders

Systemic racism impacts every person in this country. For some it
means they’re more likely to get a job interview just because of their
name. For others it means they’re more likely to be shot during a
traffic stop just because of the color of their skin. Sociologists
have been studying and documenting systemic, or institutional racism
in this country for over 100 years. There is no legitimate debate any
longer as to whether or not it exists. Ignorance of the problem can
not be claimed as an excuse, and we all take sides - either actively
working to dismantle broken systems, or passively accepting the
benefit in support of fundamentally unjust systems. There are no
innocent bystanders in this fight.

On May 25th, 2020, George Floyd, a black man, was murdered by police
in Minneapolis Minnesota. Accused of using counterfeit money to buy
cigarettes, police attempted to arrest him. He was handcuffed and
wrestled to the ground where three officers sat on him.  One on his
legs, another on his body, while officer Derek Chauvin, hands held
casually in his pockets, knelt on his neck for eight minutes.  George
Floyd struggled to breath, called out for his mom as he lay dying,
while other police officers stood by and watched. They watched calmly
while he took his last breath, watched while he died.

His murder was caught on video. The video went viral and sparked
outrage around the world. The police department took no action until
people started protesting. Initially the officers were merely fired,
which only encouraged more protests, and especially in Minnesota there
were angry riots.

Protests were organized in all major cities across the country,
demanding justice for George Floyd.  Demanding accountability from
police officers, demanding violent officers be not just given a
temporary suspension but that they be removed from their jobs and face
charges.

The attention on George Floyd’s murder also shined a bright light on
other black people who had been murdered by police or former police.
Incidents that were rarely investigated, and when they were, the
guilty officers were acquitted in highly questionable trials. Trayvon
Marton.  Elijah McClain.  Breonna Taylor. Sandra Bland  Ahmoud Arbery.
I could go on for a while, but even 8 to 10 minutes would not be
enough time.

Like I assume all of you, I was upset, and wanted my demands for
justice to be heard. I wanted our elected officials to know I demanded
accountability for law enforcement misconduct, and a redirection of
some police funding to social services that provide better benefit to
the citizens.

I believed there were at least a dozen or two like-minded individuals
in Tigard. In fact I was surprised that I hadn’t heard about or seen
any protests in our immediate area.  There had been protests in
Beaverton, Lake Oswego and Tualatin but nothing in Tigard.

I felt I had to do something, because doing nothing literally supports
the institutionalized racism that surrounds us.  There are no innocent
bystanders in this fight.

* On a Saturday I posted on nextdoor to see if people would be
interested in a peaceful family march, and a handful of people said
yes.
* I proposed we meet the following Monday, and the route we would
follow to city hall, and more people said they would be there.
* I contacted the Tigard newspaper to let them know, and the editor
said he would post it on their facebook page
* I sent an email to the mayor to let him know, and he called me within
half an hour. He and his family planned to attend and march with us.
* The police called me Sunday morning and said they had heard about the
march, and asked what my intentions were. I explained we would be
keeping to sidewalks and not blocking traffic, so would not need a
permit or permission. They were happy to hear what we planned, and
didn’t expect any problems.
* I looked for local black business owners in Tigard and could find
only one. I contacted a woman named Kasha and learned she’d had to
close her business in February, but she would come to the march.
* Kasha knew the chief of police and said she would ask her to join in.
* I made a sign, and we headed down to the meeting place.
* I thought if 50 people showed up that would be a fantastic turnout.
By the time we started walking there were over 500 people!
* It was mostly families, and everyone I saw was wearing a mask and
each group was maintaining distance from other groups.
* Kasha gave an impassioned speech about the racism she has lived with
her whole life, and how we all needed to come together to make a
positive change
* As we all walked, I participated in and overheard amazing
conversations about addressing systemic racism, about holding police
accountable, about ending qualified immunity.
* When the whole crowd made it to Tigard City Hall, Kasha took a few
more minutes to give an impromptu speech and get the crowd fired up,
then we headed back to where we started.
* [Local videographer @onelonelens](https://twitter.com/onelonelens/)
  [documented the march on
periscope](https://www.pscp.tv/w/1PlKQNvrPdnGE).

If it seems like I’m somehow well connected and a brilliant organizer,
I apologize for misleading you. The relatively huge turnout was a
reflection of what’s happening on a global scale. People in Tigard, in
fact people everywhere, want our country to do better. People are
looking for ways to join in and show their support. My suggesting we
march on that Monday was more than anything lucky timing. The people
were ready to do something, they just needed someone to say when and
where to be.

When it was all over and I had a minute to reflect and breath, I
realized how much hope the experience gave me. Until this June, it was
incredibly rare to hear random white people talking openly about
police brutality, systemic racism, or white supremacy. You might run
into that sort of thing on a liberal college campus, or at a benefit
concert for instance. But the Black Lives Matter protests that cropped
up in cities around the world after George Floyd’s murder brought
these issues to the forefront of our collective consciousness. It used
to be easy to be a white person and wave away these issues that didn’t
affect you and may have been entirely invisible to you depending on
how you were raised. We are beyond that today. Everyone is choosing a
side whether you realize it or not. 

What can you do to help?
There are lots of things you can do to affect change. The easiest is
to educate yourself. There are short youtube clips explaining systemic
racism. There are longer documentaries you could watch, like “The
13th”, about the 13th amendment. You can read books. Two great titles
I can personally suggest are “[So You Want To Talk About
Racism](https://smile.amazon.com/You-Want-Talk-About-Race/dp/1580058825/)”,
and “[The Warmth of Other
Suns](https://smile.amazon.com/Warmth-Other-Suns-Americas-Migration/dp/0679763880/)”.

You should spend some time getting to know more about your local
elected officials and learn where they stand. Join your community town
hall meetings, and don’t be afraid to ask some hard questions.

You can join a protest march, or organize one on your own - it’s not
that hard.

Most importantly though, talk about this. Racism thrives when people
are afraid to discuss it. There’s nothing shameful or scary in talking
about it. The sooner we can drag it out into the light, the sooner we
will see real change in our world.

You want positive change and an end to systemic racism, or you remain
silent, and your silence supports the broken status quo.  There are no
innocent bystanders in this fight.

