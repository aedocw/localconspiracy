Title: shoes are bad!
Date: 2009-08-19 21:52
Author: Christopher Aedo
Tags: 
Slug: shoes-are-bad

EXCELLENT book, probably one of the best I've read recently:  
[Born to Run: A Hidden Tribe, Superathletes, and the Greatest Race the
World Has Never
Seen](http://www.amazon.com/gp/product/0307266303?ie=UTF8&tag=httplocalcons-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=0307266303)![book](http://www.assoc-amazon.com/e/ir?t=httplocalcons-20&l=as2&o=1&a=0307266303)  
  
Good article (shoes are bad!), [Popular Mechanics says
so](http://www.popularmechanics.com/outdoors/sports/4314401.html)!  
  
Another good article (seriously, shoes are bad!), [Popular Science
agrees](http://www.popsci.com/entertainment-amp-gaming/article/2009-05/running-barefoot)!  
  
-Christopher  
[UPDATE: Some interesting links below]  

* [Podiatrist advocates going
  barefoot](http://www.quickswood.com/my_weblog/2006/08/athletic_footwe.html)
* [U.K.s first barefoot
  trail](http://www.ft.com/cms/s/2/fd929f02-2d75-11de-9eba-00144feabdc0.html)
* [Barefooters.org](http://www.barefooters.org/)
* [Great article at Discover.com (from
  2006)](http://discovermagazine.com/2006/may/tramps-like-us)
* [Christopher McDougall, author of Born to Run on the Daily
  Show](http://www.thedailyshow.com/watch/tue-august-18-2009/christopher-mcdougall)
* [McDougall interview on
  WHYY](http://www.whyy.org/podcast/051809_110630.mp3)
* [Born To Run website](http://borntorun.org)

