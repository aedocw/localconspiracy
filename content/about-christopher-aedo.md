Title: About Christopher Aedo
Date: 2010-02-26 01:30
Author: Christopher Aedo
Tags: 
Slug: about-christopher-aedo

For the longest time, this site has had no "about" page. It's
Christopher Aedo's personal blog, so it's not very professional or
career oriented. Just the same, there are plenty of useful tech-related
posts sprinkled in here and there (so there's definite bleed-over
between private and professional stuff).  In addition to the tech stuff,
I love to brew beer, mountain bike, run, lift, do adventure races, camp,
dive, build things and fly airplanes.  All of THAT stuff tends to keep
my mind pretty open and active, so at work I am good at thinking around
corners and bringing a unique perspective.  
  
Considering the fact that the search engines are indexing the goods, I
might as well throw in something to address the professional side of me.
 So how about an abrupt transition to a professional-profile to cover
the old "what's this guy all about on the work side of life?"  
  
Professionally, in every position I’ve held (full-time, contract based
or even advisory), I’ve become the nexus between nearly all departments.
I have a talent for presenting extremely complicated concepts in an easy
to digest manner for even the most non-technical folk. I believe more
communication is often better than less, and I always try to make sure
people understand WHY we are doing something first and foremost. As a
talented communicator, I’ve been invited to speak at multiple
conferences both in the US as well as internationally. Most recently in
Okinawa I learned the true spirit of “omotenashi” at what was probably
my most exciting speaking engagement yet. (It is very difficult to
translate into english, but it has to do with perfect hospitality -
every time I visit Japan I am reminded again of how friendly and
welcoming their culture is!)  
  
When it comes to management, I see the people I’m in charge of as humans
with amazing talents rather than simply a group of interchangeable or
easily replaced assets. The most important thing I can do is try to
foster their strengths and improve their weaknesses, and make sure we’re
striking the best possible balance of mutual benefit. Building fantastic
teams is not easy, but it’s probably been the best part of every
position I’ve held. I’m proud of what I’ve accomplished to date, but I’m
far more thankful for all the incredibly talented people I’ve been able
to work with so far.  
  
If you do find any of this to be interesting, feel free to contact me at
<doc@aedo.net>, or [via
LinkedIn](http://www.linkedin.com/in/aedo/).

