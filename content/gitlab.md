Title: Live on Gitlab
Date: 2017-02-02 18:23
Author: Christopher Aedo
Tags: gitlab, open source
Slug: live-on-gitlab

Last year when [gitlab](https://about.gitlab.com/) announced they
would offer free site hosting with custom domains (similar to github
pages) I decided it was about time I looked into moving this blog.
Until today, it had been hosted on a VM running on either Google
Compute, OVH, Vexxhost or AWS.  I think that covers all the places
I've had random VMs running, but there are probably a few others mixed
in there.  I've never been too shy about moving to a new provider to
give their services a try, and switching this blog to a site that gets
built and deployed upon commit seemed like a pretty good idea.

It was super easy to do, and [the doc on their
site](https://docs.gitlab.com/ee/user/project/pages/index.html) covered
everything you would need to know.  I hadn't been keeping [this site
in a git repo](https://gitlab.com/aedocw/localconspiracy) (I know,
shame on me!) so having one more reason to do that was also a big
plus.

Making this happen didn't require anything other than following along
with their instructions, and looking at [the example for running CI for
a Pelican site](https://gitlab.com/pages/pelican).

I'm really happy to have this site hosted on Gitlab, and I have a
feeling this is the last time I'll move it - looking forward to having
the site here for as long as I feel like keeping up with it!

