Title: Work requirement
Date: 2012-02-23 16:01
Author: Christopher Aedo
Tags: 
Slug: work-requirement

If you have a job, you need to read this.  
  

> "Although
> some meetings are inevitable, even necessary, the principle he's
> advocating here is an important one. </span>**Meetings should be
> viewed skeptically from the outset, as risks to productivity**<span
> style="background-color: white; color: #333333; font-family: calibri, tahoma, arial, sans-serif; font-size: 16px; line-height: 20px;">.
> We have meetings because we think we need them, but all too often,
> meetings are where work ends up going to die."

  
[Meetings, where work goes to die](http://www.codinghorror.com/blog/2012/02/meetings-where-work-goes-to-die.html)
  
