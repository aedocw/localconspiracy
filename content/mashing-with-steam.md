Title: Mashing with steam
Date: 2009-08-20 04:10
Author: Christopher Aedo
Tags:  steam, homebrew, beer
Slug: mashing-with-steam

I've been brewing beer for a little while now, and have been having a
lot of fun with it. As it seems to go with all my hobbies I just kept
escalating, expecting to eventually have a huge crazy brewing setup (aka
"[brew sculpture](http://www.alenuts.com/Alenuts/brutus.html)") with
three computer controlled burners, multiple pumps with plumbing and
solenoid-valves to move the liquid from stage to stage along the way,
etc. My plan was to work up to that when I started all-grain brewing,
but luckily those plans were derailed. (I say luckily because it would
have ended up costing me hundreds, if not a few grand!)  
  
As I started to learn about all-grain brewing, the concept of using a
[HERMS](http://www.homebrewtalk.com/f11/rims-vs-herms-8448/) looked like
a great approach to me. I expected I could set everything up indoors,
using electric (and would only need to pull a new 220v circuit, wouldn't
need anything else.) Since I was going to use a heating element, I might
as well control it with a computer. In the process of looking into
whether anyone else was using an [Arduino](http://www.arduino.cc/) to
control a HERMS setup (and [there are people using an Arduino in the
brew process](http://www.brewboard.com/index.php?showtopic=77935)), I
came across a pretty interesting setup. [Yuri
Rage](http://iam.homebrewtalk.com/Yuri_Rage) on homebrewtalk.com had
taken a 5 gallon keg, replaced the fittings, and was using it as a
pressurized steam vessel! [Check out the
results!](http://www.homebrewtalk.com/f51/diy-steam-mash-system-yuri-27070/)  
  
I thought it was a really neat idea, and using steam was pretty
appealing since it's an incredibly efficient way to transfer energy. The
problem I kept seeing with using HERMS is that it takes way too long to
raise your mash temp. You can go with a RIMS approach and potentially
move the temp up faster, but then you still run the risk of scorching
your wort and or having a really watery wort if you're going to pump
high volume quickly. All the approaches I saw looked overly complex and
expensive, and it started to look to me like people were putting this
stuff together not to brew better beer, but just to build a really
impressive system.  
  
I went back to the forums for info on steam, and saw everything I
needed. These posts from [Fly
Guy](http://www.homebrewtalk.com/f51/easy-steam-infusion-mash-system-25974/)
and [Beerman](http://www.homebrewtalk.com/f11/mash-steamer-2-a-21888/)
sealed the deal for me. They were both using a plain old stove-top
pressure cooker with a valve at the top, and sending the steam down to a
simple manifold made from copper tubing, into a classic Igloo cooler
mash tun! Using this setup I've been able to raise the mash temp 10
degrees in just over 5 minutes (that was with 9lbs of grain I think.) No
risk of scorching the grain since the highest temp you'll get into the
grain will be under 240 - just make sure you stir the hell out of it
while applying heat and all should be good.  
  
I put [pictures of my setup
online](http://picasaweb.google.com/christopheraedo/BeerStuff#), feel
free to email me if you have any questions at all. I'm extremely happy
with this setup, and believe I'm equipped to do just about anything I
need to with brewing, all for probably about \$200!  
  
(As an aside: I found a great writeup someone had done about why he was
not a fan of using HERMS. The guy laid out details on how fast you could
raise the mash, and why it was not the best way to brew; basically he
said you can NOT do it fast enough with a HERMS. I think he went on to
discuss using steam for mash temp control and showed how much more
efficient it was - but I'll be damned if I can find it now! Too bad, it
was really interesting, can't believe I didn't bookmark it...)  
  
More links on the topic:  

-   [This one](http://www.spykman.com/simm/what.html) is probably [the
    most elaborate setup](http://www.spykman.com/simm/simm.html) I've
    seen, looks like it works great!
-   [Choosing your heating
    method](http://www.mtbrewing.com/2009/03/25/choosing-heating-technique-for-single-infusion-mashing/)
-   Greg at [Culver City Homebrew](http://www.brewsupply.com/) pointed
    me at [this article on the subject at Brewing
    Techniques](http://www.brewingtechniques.com/library/backissues/issue2.4/jones.html),
    good reading.  

</p>

