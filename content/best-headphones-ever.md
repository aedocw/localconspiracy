Title: Best Headphones Ever
Date: 2019-06-20 17:12
Author: Christopher Aedo
Tags: 
Slug: best-headphones-ever

![image](/images/phones.jpg "bluetooth audio device and earbud headphones with custom ear molds")

Around 10 years ago I was traveling enough to where I thought I
deserved some fancy noise-cancelling heaphones. At the time, Bose was
the king of that space so I bought the [QuietComfort
2](https://amzn.to/2WHVO9k) headphones. I loved them, but I could only
keep them on for maybe two hours at a time at the very most. The
pressure on my ears bothered me, but I also found the sound pressure
of the cancellation noise got to me after a while... Maybe that wasn't
it exactly? Though they worked great on planes, even if I just had
them on with no audio going that kind of quiet hiss that was always
going on became unpleasant.  

Recently I wanted to find new headphones that sounded great and
that I could use for a few hours (or a whole day) without issues. A
conversation with a friend led me to these [Etymotic
ER23-HF3](https://amzn.to/2XevZCa) earbuds coupled with custom-molded
inserts for my ears.  I made them myself with this [Radians Earplugs
kit](https://amzn.to/2WTbpTT), and I was shocked at how amazing they
sounded.

Making the molds actually took me two tries - the first attempt I just
did not get it down into my ear canal enough for it to work well. The
second ones worked out really well and sounded incredible. I used this
set for six or seven months and at least a dozen flights. It was
great, though I suspected the fit could still be better.

I went to a local audiologist and had professional molds made
(pictured above). These were a big step up from the home-made molds I
was using.  I finally had what were by a long shot the best sounding
and most comfortable headphones/earbuds I had ever owned.  The only
drawback was the fact that they were wired. Most of the time plugging
them into my computer (for music or webex) or my phone in my pocket
(for music or phone calls) was fine, but I still snagged the wire
often enough for it to be annoying.

There are lots of Bluetooth adapters I could use but not surprisingly
the cheap one I bought for $12 did not sound great.  It was certainly
acceptable for conference calls but beyond that, it kind of sucked.
The problem was the audio codec, and none of the cheap ones support
APT-X. Also I hadn't really thought about Bluetooth audio codecs until
after I got that cheapie and found the quality sucked.

I had heard of adapters from Fiio and thought [this comparison/review
of several Bluetooth audio
adapters](https://www.audiophile-heaven.com/2019/01/fiio-bluetooth-freedom-review.html)
was really good.  I had already seen the [Fiio BTR1K](https://amzn.to/2MMjfyL)
recommended multiple places and was happy to see it stood up well in
the comparison. 

When I first tried it out though, I could not get my machine (OSX) to
use aptX codec, it kept only connecting with the SCO codec (on OSX you
can see what codec a Bluetooth device is using by holding down option
while clicking the Bluetooth dropdown in your menu-bar, then hovering
over whichever device you want to see).  Turns out because the BTR1K
has a built-in microphone there was not enough bandwidth for the
better codec.  Switching the sound-input device to "Internal
Microphone" was all it took, a few seconds later it switched to the
better codec and suddenly the audio sounded nearly as good as being
directly plugged in.

Including the custom ear molds (and some discounts here and there) the
whole setup including Bluetooth adapter cost about $300.  It's not
cheap, but I don't expect to replace them any time soon.  Honestly
they're so comfortable and great sounding I feel like it was a pretty
worthy investment.

