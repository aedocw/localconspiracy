Title: Better living through chemistry
Date: 2012-09-26 03:24
Author: Christopher Aedo
Tags:  downfall of humanity, conspiracy, collusion, beating a dead horse, improvement, corruption, be better, corporate shill
Slug: better-living-through-chemistry

It's absolutely amazing what big pharma gets away with, but it seems
like so few people are aware or the least bit concerned.  We might never
know [how many people Merck & Co. killed with
Vioxx](http://www.theweek.co.uk/us/46535/when-half-million-americans-died-and-nobody-noticed)
(maybe half a million?).  The more you read, the more you find one scary
story after another, with no end in sight.  [The FDA won't do anything
about it (safety last is their
motto!)](http://www1.cs.columbia.edu/~unger/articles/safety.html), our
only hope is that people like Ben Goldacre will get more recognition and
exposure.

[Ben Goldacre](http://en.wikipedia.org/wiki/Ben_Goldacre) (a writer,
doctor and psychologist) knows medicine. He's trying to shine a bright
light on the deadly safety issues surrounding the failed regulatory
processes meant to ensure the pharmaceutical industry stays honest. [In
a recent article in The
Guardian](http://www.guardian.co.uk//business/2012/sep/21/drugs-industry-scandal-ben-goldacre/print?mobile-redirect=false)
he tells an incredible story about prescribing a drug he thought was
safe, when in fact the manufacturer knew not only did it NOT help
children battle depression, it actually made them more likely to commit
suicide!  

> "In October 2010, a group of researchers was finally able to bring
> together all the data that had ever been collected on reboxetine, both
> from trials that were published and from those that had never appeared
> in academic papers. When all this trial data was put together, it
> produced a shocking picture. Seven trials had been conducted comparing
> reboxetine against a placebo. Only one, conducted in 254 patients, had
> a neat, positive result, and that one was published in an academic
> journal, for doctors and researchers to read. But six more trials were
> conducted, in almost 10 times as many patients. All of them showed
> that reboxetine was no better than a dummy sugar pill. None of these
> trials was published. I had no idea they existed."

He [wrote a
book](http://www.amazon.co.uk/dp/0007350740/ref=nosim?tag=bs0b-21) on
this topic that's already getting rave reviews. It's also getting panned
by industry shills who would much rather the public keep their heads in
the sand when it comes to safety and efficacy of the drugs they're
trying to sell us. In his intro to the book, he sums up the premise in a
single (horrifying) paragraph "that will seem so absurd – so ludicrously
appalling – that when you read it, you’ll probably assume I’m
exaggerating."  

> "Drugs are tested by the people who manufacture them, in poorly
> designed trials, on hopelessly small numbers of weird,
> unrepresentative patients, and analysed using techniques which are
> flawed by design, in such a way that they exaggerate the benefits of
> treatments. Unsurprisingly, these trials tend to produce results that
> favour the manufacturer. When trials throw up results that companies
> don’t like, they are perfectly entitled to hide them from doctors and
> patients, so we only ever see a distorted picture of any drug’s true
> effects. Regulators see most of the trial data, but only from early on
> in its life, and even then they don’t give this data to doctors or
> patients, or even to other parts of government. This distorted
> evidence is then communicated and applied in a distorted fashion. In
> their forty years of practice after leaving medical school, doctors
> hear about what works through ad hoc oral traditions, from sales reps,
> colleagues or journals. But those colleagues can be in the pay of drug
> companies – often undisclosed – and the journals are too. And so are
> the patient groups. And finally, academic papers, which everyone
> thinks of as objective, are often covertly planned and written by
> people who work directly for the companies, without disclosure.
> Sometimes whole academic journals are even owned outright by one drug
> company. Aside from all this, for several of the most important and
> enduring problems in medicine, we have no idea what the best treatment
> is, because it’s not in anyone’s financial interest to conduct any
> trials at all. These are ongoing problems, and although people have
> claimed to fix many of them, for the most part, they have failed; so
> all these problems persist, but worse than ever, because now people
> can pretend that everything is fine after all."

Start up a conversation with your friends, the only way to get more
people thinking about this stuff is to get more people talking about it!
