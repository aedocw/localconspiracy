Title: Migrating from a hosted google domain
Date: 2022-02-20 20:00
Author: Christopher Aedo
Tags: 
Slug: migrate-from-gmail

These instructions are specifically intended for folks moving from a
domain hosted by Google (Google Suite, set up back in 2006 when it was
free). In this case we have a bunch of email addresses at a specific
domain (mydomain.com will be used in the example bewlow), and we want
to continue using email addresses such as me@mydomain.com. The easiest
way to do this will be by using email forwarding from namecheap.com,
and forwarding the email to regular gmail accounts. Gmail will also
let you send mail as though it came FROM that address as long as you
are able to confirm you are in control of that address.

1. First off, Set up 2-factor-auth on your EXISTING mydomain.com account
    1. Click Manage your Google Account
    1. Click Security on the left
    1. Under "Signing in to Google", click 2-Step Verification

1. If you do not already have a gmail account, set one up. It's free
and easy, just LOG OUT OF ALL YOUR GOOGLE ACCOUNTS, then [go to
gmail.com](https://gmail.com) and click "Create Account". When setting up
that account, be sure to enable 2-factor authentication (you will need this
to be enabled later).

1. On your NEW gmail account (which you will have your mydomain.com
email forwarded to), you will need to Create an "App
Password" [(follow these steps)](https://support.google.com/accounts/answer/185833) and **SAVE
THIS PASSWORD SOMEWHERE**. You will need it later.

1. Set up your NEW gmail account to send mail as your custom email
address (me@mydomain.com for this example). 

    1. In Gmail, click the gear icon in upper-right area (it will say
   "Settings" when you hover over it)
    1. Click "See all settings" 
    1. Select "Accounts and Import" tab
    1. In "Send mail as" section, click "Add another email address"
    1. In the "Add another email address you own" window that opens, enter
   your custom email address (me@mydomain.com), leave "Treat as an alias"
   checked, then click "Next Step"
    1. For SMTP information, use:
        1. SMTP Server: smtp.gmail.com
        1. Port: 587
        1. Username: (your NEW gmail account from step 1)
        1. Password: (the "App Password" you created in step 2)
        1. Choose "Secured connection using TLS"
    1. When you click "Add Account", a code will be sent to your @mydomain.com
   account. Enter that code in the box then click "Verify"
    1. In "Accounts and Import/Send mail as:" section, click "Make default"
   next to your me@mydomain.com email.
    8. Scroll to the bottom and click "Save Settings"

1. Forward mail from your old account to your new account
    1. In your mydomain.com account, click the gear icon and go to "See all settings"
    2. Select "Forwarding and POP/IMAP" tab
    3. Next to "Forwarding", click the "Add a forwarding address" button
    4. Enter your NEW gmail.com address in the "Add a forwarding address" popup
    5. A new window will open titled "Confirm forwarding address", click "Proceed"
    6. Check your mydomain.com email for a verification code, and enter that in the "confirmation code" box, then click Verify.
    7. In the forwarding section now, click the button next to "Forward a copy of incoming mail" to your new gmail address, and select "archive old domain Mail's copy"
    8. Scroll to the bottom and click "Save Settings"

At this point, you can use your NEW gmail account exclusively!

What about transferring your old mail to your new account? There are
a few different ways to do this but the safest thing to do first is to
go to use Google Takeout to download a copy of your mail, contacts and
calendar. You can also download photos that you have saved on Google
but you should get those through their own Takeout request.

1. Go to [Google Takeout](https://takeout.google.com)
2. Ensure you are using the correct account - click the circle icon in
the upper-right corner and choose a different account if your mydomain.com
account is not selected.
1. Under "CREATE A NEW EXPORT" click "Deselect All"
1. Scroll down and check the box for the following:
   - Calendar
   - Contacts
   - Mail
1. Click "Next step"
1. Click "Create export"


