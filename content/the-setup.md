Title: The Setup
Date: 2014-02-05 16:30
Author: Christopher Aedo
Tags:  thingsilike, necessary skills, appletv, be better
Slug: the-setup

Over the years I have been continually refining my home technology
stack.  It's in the best state it's ever been, so I figured I should
document it.  I'm looking forward to looking back at this post in a few
years to see what's evolved, and what's still the same.  
  
External connection - Verizon FiOS.  It's usually pretty good.
 Unfortunately it seems like they keep edging the price up a few dollars
at a time.  But there's no alternative if I want reasonable speed.
 Wouldn't it be nice if there were more options?  [There should
be](http://www.dataroads.org/blog/)!  
  
Firewall/WAP - [Home-built
ALIX/pfSense](/2014/02/you-need-better-firewall.html).
 It's just plain awesome.  
  
Cell phone - We're using [Republic Wireless with the
Moto-X](https://republicwireless.com/phones/moto-x).  Also just plain
awesome.  The phone is the best I've ever used.  The service isn't
perfect, but it's close, and it gets better all the time.  The price is
unbeatable.  
  
Land-line - Cancelled it so many years ago I don't even remember when.
 We do have phones in the house that are fantastic, and the service is
free.  I've had a google voice number since the early early days, and
bridge it with an [OBiTalk](http://www.obitalk.com/) VoIP adapter.  This
[\$38 adapter from
Amazon](http://www.amazon.com/OBi100-Telephone-Adapter-Service-Bridge/dp/B004LO098O/ref=sr_1_1?ie=UTF8&qid=1391447948&sr=8-1&keywords=obitalk)
works beautifully.  
  
TV/Video - [Cancelled satellite
subscription](/2009/05/tivo-is-for-suckers.html)
six or seven years ago and have been using a [2nd gen
AppleTV](/2011/02/watching-tv.html) for
quite a while.  It's bad for your brain but I just can't quit it.  
  
Video content and automation - The non-Netflix content is served off a
RaspberryPI with a 1.5tb USB drive attached to it.  I'm pretty sure it's
close to the most energy efficient solution.  In the past I've run
full-size PCs with lots of internal disks, or once (for a year?) the
content lived on a Drobo.  Operating systems experimented with included
OpenSolaris, FreeBSD and Linux.  Always lots of moving parts, and over
time, usually a hassle of one sort of another.  Since moving to the pi,
I've also added an X10 USB dongle (so a simple script turns on the
outside light at sunset, turns it back off at midnight - christmas
lights were similarly managed, etc.)  
  
Retrieving content - OK, this part is being shared solely for academic
purposes.  (I am sure it's against the rules to grab torrent content
because that's what pirates do, obviously.  Even if it's just a show
that was broadcast over the open airwaves, and could have been delivered
to you nearly identically by a neighbor with a VCR.)  I am [running this
script](https://github.com/aedocw/TVShowGrabber) on the raspberry pi,
which regularly checks for new episodes of any show we specify, and if
found, automatically starts up the download.  
  
Backups - I used to back the laptops up to the file server, which was
why I always had a box set up with raid-5 or raid-z.  Once upon a time
though, two drives in the drobo failed at the same time and it took me
way longer than it should have to get that data back.  After that, I
switch to [CrashPlan](http://www.code42.com/crashplan/).  It's been
awesome, and has been used to recover all content from lost hard drives
twice.  Because it saves incremental snapshots as well, I've even used
it to pull back a previous version of a file on a few occasions.  We
have a family plan shared with my family and a friend, so the actual
cost per machine being backed up is about \$2/month.  
  
All together, it's a very simple setup, and the hardware components
should last a good long time.  It's also completely portable (only
requirement is an internet connection).  The TV side is also easily
expanded - if you have multiple TVs, you still have a central repository
of content (just need additional AppleTVs!)  Grabbing shows is
completely automated just like with a fancy DVR (though I'll admit it's
not as convenient - but maybe the next step will be for me to bolt a
web-UI in front of the show grabber!)  
  
  

