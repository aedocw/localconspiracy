Title: a start
Date: 2009-08-19 17:05
Author: Christopher Aedo
Tags:  antisocial networking, random
Slug: a-start

I've had a few different blogs over the years, but I've never paid much
attention to them, and eventually abandoned them generally due to lack
of (personal) interest. With luck, I won't give up on this one!  
  
I don't imagine there will be an overriding theme, I'm just hoping I'll
post some interesting things now and then without seeming narcissistic
(I draw the line at a blog, and steadfastly refuse to participate in
tweets and other madness; honestly, the world at large does not need to
know you're on your way to starbucks. If you're concerned with letting
your friends know where you're headed, THEN F\*ING CALL THEM!)  
  
Uh oh, I better stop here. Maybe my first real post will be about how
things like twitter make me feel sad for the participants...  
  
-Christopher

