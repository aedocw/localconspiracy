Title: Reading
Date: 2011-01-04 00:03
Author: Christopher
Tags:  kindle, books, reading
Slug: reading

I've been meaning to post little links more often, to interesting sites
I find or books I really like (instead of saving up for one really big
post every few months). This one will be a little bit long just because
I've got a few things to note - in the future, most additions to Local
Conspiracy will probably be pretty short and consist of stuff like "I
just finished this book and loved it! You should read it!"

Before I mention any specific books though, I have got to give a giant
shout out to the [Kindle](http://goo.gl/xLNQD). I got my wife one for a
late birthday gift, and immediately realized I was going to need to get
my own. It's not super fancy, allowing you to play games and watch
movies like an iPad, but it is a LOT nicer to your eyes. Keep in mind
when you're staring at ANY LCD screen, you're staring into a dim light.
Your eyes don't like doing that, so the more time you spend staring a
screen (your laptop, TV or iPad), the more strain you put on your eyes.
The Kindle uses an e-ink display that relies on purely reflective light
(exactly like reading a regular book). It's a LOT easier on your eyes.
If you spend much of your day in front of a computer screen at work, and
then go home and spend a few more hours staring at your TV, you should
do your eyes a kindness and give them a break by reading a book
(physical, or digital on your Kindle).

There are lots of advantages to using a Kindle to read books (and
believe me, the ability to get a book RIGHT NOW is pretty intoxicating!)
One advantage I hadn't thought about until I was using it though was the
fact that you can easily hold the book and turn pages without making a
sound or shifting around, or even using two hands. That comes in
EXTREMELY handy when you've got a sleeping baby in your arms.

OK, enough of that. On to a few books I've read recently, which I highly
recommend...

[Unbroken: A World War II Story of Survival, Resilience, and
Redemption](http://goo.gl/rjLuV) (by Laura Hillenbrand) - Excellently
written, a fascinating story of WWII as seen (primarily) from the eyes
of a man who was captured by Japanese soldiers. This book was hard to
put down.

[WRONG: Why experts keep failing us -- and how to know when not to trust
them](http://goo.gl/wooLC) (by David H. Freedman) - This book should be
required reading for EVERYONE. If you're not familiar with how medical
studies (or many other scientific studies for that matter) are
documented and published, this book will be a real eye opener. There's a
ton of useful information in this book, and it goes a long way towards
inspiring some necessary critical thinking.

[Packing for Mars: The Curious Science of Life in the
Void](http://goo.gl/jEYNF) (by Mary Roach) - a fun read detailing a
great many of the interesting considerations that go into preparing men
(and animals) for space travel.  
