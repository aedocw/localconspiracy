Title: 1984, surveillance framework and your permanent record
Date: 2012-08-16 19:40
Author: Christopher Aedo
Tags:  downfall of humanity, antisocial networking, beating a dead horse, improvement, corruption
Slug: 1984-surveillance-framework-and-your-permanent-record

Now THIS one definitely sounds appropriate for a blog with "conspiracy"
in the title!  I've been collecting a bunch of related stories for
several months now with the intention of turning it into a blog post.
 Sadly I've had way too much other stuff to do lately, so I thought I
might as well just throw this out there for the two or three people who
check out my blog now and then.  
  
For the tl;dr folks: the confluence of social networking and big data
means all those people sharing every bit of their private lives with the
world at large (or even when they think they're only sharing with a
small group of friends) are giving corporations and governments a whole
ton of data they should not have.  For the people giving up the data,
it's going to mean what they though was private will be scrutinized by
employers, insurers, marketers and governments.  
  

> "Why should I care about surveillance if I have nothing to hide?"
> ([from ACLU
> blog](http://www.aclu.org/blog/technology-and-liberty-national-security/plenty-hide))

> "One can usually think of something that even the most open person
> would want to hide. As a commenter to my blog post noted, "If you have
> nothing to hide, then that quite literally means you are willing to
> let me photograph you naked? And I get full rights to that
> photograph—so I can show it to your neighbors?"" ([from
> Chronicle](http://chronicle.com/article/Why-Privacy-Matters-Even-if/127461/))

> "...he visited Disneyland, and went on a ride, the theme park offered
> him the photo of himself and his girlfriend to buy – with his credit
> card information already linked to it. He noted that he had never
> entered his name or information into anything at the theme park, or
> indicated that he wanted a photo, or alerted the humans at the ride to
> who he and his girlfriend were – so, he said, based on his
> professional experience, the system had to be using facial recognition
> technology. He had never signed an agreement allowing them to do so,
> and he declared that this use was illegal. He also claimed that Disney
> had recently shared data from facial-recognition technology with the
> United States military.  
> Yes, I know: it sounds like a paranoid rant.  
> Except that it turned out to be true." ([from The
> Guardian](http://www.guardian.co.uk/commentisfree/2012/aug/15/new-totalitarianism-surveillance-technology))

  
Insurance companies already review what you're sharing publicly to
determine whether or not you're a risk worth taking on.  (That facebook
pic of you eating a krispy-kreme probably means you're an unhealthy
slob, better charge you double for that health insurance!)  

> "A SWISS life-settlements firm called Rigi Capital Partners (RCP)
> recently considered buying the life-insurance policy of an elderly
> woman apparently suffering from dementia. RCP would take over payment
> of the policy premiums and receive the full death benefit when she
> passed away. Her medical records revealed that she had forgotten even
> her son's birthday. But Robin Willi, RCP's owner, searched Facebook to
> find out more about her. Her profile suggested she had a vibrant
> social life, not dementia. Reckoning that she was much healthier than
> she wanted to appear, Mr Willi did not offer to buy her policy."
> ([from The Economist](http://www.economist.com/node/21556263))

Speaking of facebook, [they know way more about you than would
imagine](http://www.technologyreview.com/featured-story/428150/what-facebook-knows/),
and they are keeping it forever.  Eventually [any video or picture you
show up in](http://face.com/blog/facebook-acquires-face-com/) even
briefly will be linked back to you.  It won't require your approval, and
it won't even be made public, but the link is there for them to use as
they please.  Combined with the location data tied to that stuff, it
will get harder and harder to leave your house without every thing you
do being logged and analyzed.  Gathering all this information about you
and linking it all together helps the people with that info determine
all kinds of stuff you would probably rather they not know.  Where do
you like to spend your time?  Who do you hang out with?  Are you sick?
 Pregnant?  This is just scratching the surface...  

> "The piece described how Target identifies customers who are pregnant
> (sometimes before their own family members know) by tracking
> customers’ purchases and identifying patterns in their behavior. It
> then uses that insight to sell them baby-related goods." ([from ACLU
> blog](http://www.aclu.org/blog/technology-and-liberty/eight-problems-big-data))

The schools can't afford to pay for the same level of access as the
advertisers and governments, but that doesn't stop them from [trying to
force their way in
anyhow](http://redtape.nbcnews.com/_news/2012/05/18/11747289-school-officials-facebook-rummaging-prompts-moms-privacy-crusade).  
  
Finally, one of the most important videos you should watch with your
family.  Why you should not talk to the police, even if you think you
are completely innocent and have nothing to hide.  Explained by a
fantastic attorney, and confirmed by a seasoned police detective.  [Part
1](http://www.youtube.com/watch?v=i8z7NC5sgik), and [Part
2](http://www.youtube.com/watch?v=08fZQWjDVKE).

