Title: Get up, stand up!
Date: 2010-08-19 05:09
Author: Christopher
Tags:  health, exercise, fitness
Slug: get-up-stand-up

![image](/images/3-standups.JPG "three stand up desks")

Last year a coworker and friend ([Rocco](http://www.privetera.com/)) set
up his workspace so he could use the computer standing up. It wasn't the
first time I'd heard of people doing that, but he was the first person I
knew who was giving it a try. After a few days, he swore by it. He's a
smart guy, so I started to give it some serious thought.  
  
A few months later another coworker and friend
([Carol](http://studiosoh.com/)) said she wanted to get a stand-up desk,
and she thought I would be into it too. She was right. I started reading
more about it and found lots of good information in support of this
(links below) and I was sold. It's been about three months now since I
switched, and I'll never go back to sitting down at the computer - this
is biggest positive health change I've made this year (last year it was
[ditching my
shoes](/2009/08/shoes-are-bad.html)).  
  
My core is stronger than it's ever been, I haven't had any back issues
since the switch, and I've definitely got more energy than ever.  
  
We are using the [FREDRIK desk from IKEA](http://bit.ly/3V5GJF) (\$149),
and [GelPro Chef's Mats](http://amzn.to/9oRJJU) from Amazon (\$99). The
desk is perfect because it makes it easy to get the monitor up at eye
level (keeps your head up which means the blood flows more easily to the
brain and you'll have less eye and neck strain). You probably need a
nice gel mat to stand on as well, and the GelPro mat is PERFECT, makes
all the difference in the world.  
  
This is definitely catching on at work (that picture up top is three
standups in a row, and there are another three people here also using
the same setup!) I'm hoping more of my friends give it a try too.
Honestly, this is the kind of stuff that makes your whole life better!
(Also, get more Vitamin-D while you're at it!)  
  
  
The links:  
* Longer you sit, the shorter your life: <http://yhoo.it/d80NWl>
* Rethinking the desk: [http://blog.trailmeme.com/2010/04/rethinking-the-desk/](http://blog.trailmeme.com/2010/04/rethinking-the-desk/)
* NY Times: <http://nyti.ms/crnR9A>
