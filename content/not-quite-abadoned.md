Title: Not Quite Abandoned
Date: 2015-07-13 12:40
Author: Christopher
Tags: random
Slug: not-quite-abandoned

Holy smoke it's been almost a year since the last time I added
something new here! That is not at all because I haven't had
anything new to talk about.  Rather, there's been too much going
on and I haven't had even half a chance to circle back and write
up a blog post.

I do wonder where those folks who write constantly find time. I
know when I end up with extra time it either means I can finish
doing something I've needed to do to the house (add a new
outlet, make a frame for a mirror, build a retaining wall, etc.)
or else do something with the kids.  Usually that means go
exploring with them. Sometimes I have "spare" time at the end of
the day but usually if I'm awake, then I'll read.

I DID finally take a little time to convert this blog to Pelican
(away from Blogger). Some links might be broken, and I still have
a little housekeeping I want to do (like put the content in a git
repo and set it to build and publish automatically on each commit
for instance).  But so far, I think the transition is looking
pretty good!

In general though, I'm sorry to say adding new things to the blog
is pretty low priority at the moment. Maybe I need to set aside
some time once a week to write up some of the interesting stuff I
ran across the previous week :)
