Title: Watching TV
Date: 2011-02-15 07:06
Author: Christopher Aedo
Tags:  bittorrent, appletv, dvr
Slug: watching-tv

So as much as I wish I didn't want to watch TV, sometimes I just can't
help it (it generally feels like I'm shutting my brain down when I watch
TV and sometimes I just need that). We [cancelled satellite a few years
ago](/2009/05/tivo-is-for-suckers.html),
though we still downloaded a few TV shows each week because quitting
cold turkey was just too hard. Back then the system was basically a
computer downloading TV shows via bittorrent, and a hacked AppleTV
streaming the content on our TV.  
  
It's about time for a quick update on the system, because it's really
been pretty great. The only real change since my last post is that we've
replaced the first generation AppleTV with a cheaper, more powerful, and
significantly smaller second generation AppleTV.  
  
The new AppleTV is [easy to
hack](http://support.firecore.com/entries/387605), and now that the fine
folks behind[XBMC have ported their
software](http://wiki.xbmc.org/index.php?title=Install_XBMC_on_ATV2) to
 this hardware, you can do everything you would want to do with the
AppleTV.  
  
What's the best new thing I've found? Adding the [TED Talks RSS
feed](http://www.ted.com/pages/198).  Now we can "flip channels" when we
feel like watching TV, and we're NOT really shutting down our brains!  
  
Write me if you have any questions about "adjusting" your AppleTV to
better suit your own needs!

