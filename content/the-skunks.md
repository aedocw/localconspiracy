Title: The Skunks
Date: 2016-08-07 17:31
Author: Christopher Aedo
Tags: skunks, random
Slug: the-skunks

![image](/images/pepelepew.jpeg "cartoon skunk")

One night a few months after we moved into our place in Portland
we noticed a bit of a skunk smell. It wasn't too strong, and we just
assumed there was a skunk in the woods and something had scared
it. It didn't take too long for the smell to go away, and we
didn't think about it again. A few weeks later we took
a vacation and had a relative stay in our house while we were
gone. She told us that a few nights after we left there was a REALLY
strong skunk odor in the house suddenly, and it was enough to
stink up the whole place. She said she opened all the windows
and it took another day or two before the smell was gone. At the
same time our beagle had gone a little crazy and was trying to
dig through the floor in our bathroom.

That was definitely not a good sign.

Within a few days of our return we got a whiff of skunk again,
and the beagle was doing her best to get under the house through
the bathroom floor. We calmed her down, and eventually went to
bed. Around 2am we heard some scratching and rustling sounds
coming from an air conditioning duct in the floor of our room. The beagle
leaped off the bed and started frantically scratching at the
vent. Eventually the noises stopped, the dog calmed down, and we
went back to sleep.  At this point I was still trying to convince
myself it might not be a skunk...

The noises continued a few days later, and by then I was sure we
had a problem. Looking everywhere around the exterior of the house I found the
only place they could have been getting in. Under the deck at the
front door it looked like something had dug a little hole to get
under the deck, and from there they were getting under the house.
I tried putting cinder blocks and river rocks where it looked
like they were coming and going but they just dug new holes.

I searched around the 'net for suggestions on how to keep the
skunks away. I still thought they were just visiting under our
house for some reason (maybe we had tasty grubs or something),
because I honestly did not want to believe there was a skunk
trying to live under our house. Turns out they nest in big
groups, as many as 14! Reading that had me really worried that
maybe we had a dozen skunks living under the house. By now I was
ready to seek a pro, and we found a guy who said he would come by
and see if it was a job he was willing to do.

When Jeff came over I showed him where I thought they were coming
and going, and he was pretty sure we had skunks. We agreed on a
price (which was totally reasonable!) and he was hired. Turns out
I was lucky and caught him on a good day; normally he turns down
skunk removal jobs (same with "angry raccoon in my attic" jobs). I
think it's a pretty sensible policy, I would do the same.

That day he left a trap by the deck baited with a granola bar.
Before it even got dark, we had our first capture! Of a
squirrel.  When I opened the trap that thing shot out like a
rocket. I reset the trap, put some new bait in and crossed my
fingers.

Sure enough, by 10pm or so I noticed the trap was sprung and
inside was an unhappy skunk. Surprisingly it hadn't sprayed or
anything, so at least we did not have to deal with a stink like
that.  The next morning Jeff came to fetch the critter and left a
new trap.  He drove the skunk about 60 miles away and released
it into the woods.

The next night we had a repeat, except whatever got trapped was
making a whole lot more noise.  Lots of scratching sounds and
even sounds of the trap being moved around.  From inside the
house we shined a light on the trap and saw one big skunk in the
trap plus one smaller adolescent skunk scratching all around the
base of it trying to help free the captive one.  Eventually the
smaller one left, and the next day Jeff collected the trapped
skunk and repeated the release process.

The third night we caught one more skunk, but caught no others
for a few more nights.  We actually might have caught four in
total but I can't really remember for sure.

![image](/images/deck-opened-up.jpg "wooden deck with section removed")

As soon as I thought the skunk situation was sorted out, I opened
up the deck and filled in the gap the skunks were using.  I
used several cinder blocks plus two bags of cement to be sure
they did not have another shot at it.  

![image](/images/skunk-hole-plugged.jpg "foundation with cement poured")

I also lined the
perimeter of the deck with a thick layer of river rocks so
any further attempts to get under there would be difficult
and obvious.  Since then, we haven't had a single problem with
animals (unfriendly or otherwise) trying to nest under our house.
Hopefully that never changes!


