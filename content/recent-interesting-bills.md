Title: 10 Bills You Should Read
Date: 2017-03-06 18:45
Author: Christopher Aedo
Tags: government, corruption, downfall of humanity, fail
Slug: recent-interesting-bills

I try not to post too much political stuff here unless I think it's
something useful and interesting.  In this case, I've seen this list
of bills proposed in the US house of representatives several places.
All the times I've seen it however, it's just text (and often on
twitter it's an IMAGE of text).  No links, no more information, just
basically "here are some numbers and scary titles of bills!"

For my own information I wanted to find the actual bills and their
titles, initially expecting the titles to be less scary, or the
numbers not actually matching up.  I guess I expect "fake news"
everywhere I look these days.  As it turned out, the numbers and
titles matched up.  The content is pretty scary in my opinion, and
represents a push to create an America that is not consistent with the
constitution we were founded upon.  At least these efforts are in the
public and are being recorded for the sake of history.  What follows
is the list I came across plus links to the actual bill on
congress.gov.  If for some reason that content is later removed I will
update these links to point to their archived versions at [the internet
archive](https://web.archive.org).

* [HR 861 - Termination of the Environmental Protection Agency](https://www.congress.gov/bill/115th-congress/house-bill/861/text)
* [HR 610 - Choices In Education Act](https://www.congress.gov/bill/115th-congress/house-bill/610/text) (vouchers for public education)
* [HR 899 - Termination of the Department of Education](https://www.congress.gov/bill/115th-congress/house-bill/899/text)
* [HJR 69 - Repeal department of interior rule protecting wildlife](https://www.congress.gov/bill/115th-congress/house-joint-resolution/69/text)
* [HR 370 - Repeal the Patient Protection and Affordable Care Act](https://www.congress.gov/bill/115th-congress/house-bill/370/text)
* [HR 354 - Discontinue Funding for Planned Parenthood](https://www.congress.gov/bill/115th-congress/house-bill/354/text)
* [HR 785 - National Right-to-Work Act](https://www.congress.gov/bill/115th-congress/house-bill/785/text) (ends or disables unions)
* [HR 83 - Prohibit the receipt of Federal financial assistance by sanctuary cities](https://www.congress.gov/bill/115th-congress/house-bill/83/text)
* [HR 147 - Prenatal Nondiscrimination Act](https://www.congress.gov/bill/115th-congress/house-bill/147/text)
(bans abortions based on sex - i.e. if you wanted a boy but are
pregnant with a girl instead, an abortion on that basis alone would be
illegal.)
* [HR 808 - Sanctions Against Iran](https://www.congress.gov/bill/115th-congress/house-bill/808/text)
