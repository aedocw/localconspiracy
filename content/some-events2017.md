Title: Next Few Talks in 2017
Date: 2017-04-21 18:38
Author: Christopher Aedo
Tags: speaking, homebrew, open source
Slug: nextfewtalksin2017

![image](/images/pdx-aus-ewr.png "map of united states showing route from portland to new york to dallas to austin to san francisco then back to portland")

I'm sitting here on a plane headed home, finally, after what has been
a pretty long week of travel for work.  All of it has been good stuff,
and even included the opportunity to share the stage with IBMs Chief
Digital Officer Bob Lord. During his quarterly "all hands" town hall
meeing, I was invited up to talk to him about what we are doing with
respect to developer advocacy at IBM. It's pretty gratifying to be
part of such a massive company but realize I still have a voice and am
actually empowered to make changes.

![image](/images/townhall.png "christopher aedo sitting with bob lord during an IBM town hall")

Anyway, on this plane right now it has occured to me that I do a pretty
terrible job of mentioning when I'm going to be speaking somewhere, or
doing something else really interesting relating to my job.  So I'm
going to do a better job at that starting now!

The first thing to share is that I get to do [a keynote at OSCON
2017](https://conferences.oreilly.com/oscon/oscon-tx/public/schedule/detail/61296)!
True, it's a sponsored keynote, but it's kind of a neat deal.  IBM has
been sponsoring [OSCON](https://conferences.oreilly.com/oscon/) for a
long time.  As part of that sponsorship they get a 10 minute
"sponsored" keynote slot.  Previous talks have been met with mixed
reactions, largely because they tend to be very product focused.
As part of our work to improve the way the world of developers sees
IBM, I have been given the opportunity to take that keynote slot this
year.  I'm super happy to have the chance, and I'm also really excited
to talk about something that's got nothing to do with any IBM
products.  I think folks will enjoy it, and I really hope I get the
chance to do more talks like it.  I'm going to post something soon on
the same topic I'll be speaking about, but here's the abstract if you
didn't feel like following the link above:

> Why choose open infrastructure?
> Open Source isn’t winning, it’s won. In the last decade, we’ve seen an
> incredible explosion in open source software. Massive projects have
> been developed in the open, on open operating systems, using open
> languages and compilers. But, Christopher Aedo asks, was all the
> infrastructure open as well?
> 
> Source code management, CI pipelines, chat services, and IaaS all have
> open alternatives, but the majority of projects are developed on
> closed and proprietary infrastructure. Christopher explains how to
> make open choices with greater community impact.

I also recently had a talk submission accepted at the [OpenWest 2017
Conference](https://www.openwest.org/).  The topic of my talk is
"Brewing Beer with Linux, Python and a RaspberryPi".  It's a topic that
is near and dear to my heart, and I'm really looking forward to taking
the stage and sharing the cool stuff I've been using to make beer
lately.

Hope to see you at OSCON and OpenWest!
