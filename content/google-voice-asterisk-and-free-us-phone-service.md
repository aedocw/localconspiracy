Title: Google voice, Asterisk, and free US phone service
Date: 2012-05-13 17:12
Author: Christopher Aedo
Tags:  voip, google voice, asterisk, free phone
Slug: google-voice-asterisk-and-free-us-phone-service

[**EDIT 20120-05-13**: You can follow these steps (though by now they're
already fairly outdated), and end up with a mostly functional home phone
system connected to a free Google Voice number.  It will break often, as
Google makes changes, and your Asterisk box needs updating, and who
knows what else.  OR, you can skip all that, spend \$45 and be done with
it.  That's what I did.  This little box ([OBi110 VoIP service
bridge](http://www.amazon.com/OBi110-Service-Bridge-Telephone-Adapter/dp/B0045RMEPI))
is one of the best purchases I've ever made.  HIGHLY recommend it.  One
trips abroad (Europe and Manila) I used free hotel wifi to make calls
over my iPhone with the free OBiTALK client, and as long as the
connection was moderately decent, the quality was excellent!]  
  
In 2009 Google purchased a company offering VoIP phone numbers called
GrandCentral. That allowed Google to start giving away US phone numbers.
The idea was that you would get this new phone number and you could keep
it forever - and any calls to that phone number would forward to the
phone(s) of your choosing. It also offers voice mail service, and when
you get a message, you can have a transcript of that message sent to you
via email (as well as the message itself, as an audio attachment.) One
of the most powerful features was that you could set up several phone
numbers to forward the call to (your cell phone, home phone and office
phone). Any incoming call to your Google Voice number would ring on ALL
phones at the same time, and the call would be routed to whichever phone
you picked up first!  
  
It's a great idea, but it is kind of a hassle to use it for making
outbound calls from a regular phone- and if you're not making calls from
that number, it's much less likely people will remember to use that one
when trying to reach you. You CAN make phone calls from your computer
(right in your web browser when connected to gmail), but not too many
people like to do that - and it's certainly not convenient for visitors
or others in the house who need to use the phone. There was another
company (Gizmo5) that allowed you to connect to your GrandCentral number
via "voice over IP", so using the two systems was a good alternative to
VoIP services like Vonage. Unfortunately Google bought Gizmo5 and
suspended new signups while they worked on integrating their
technology.  
  
There were some workarounds that let you use the free open-source phone
switching system Asterisk to connect to your GV number from a VoIP
phone, but they still required phone service through another provider,
so it was going to cost you a subscription one way or another.  
  
Lucky for us, the latest version of [Asterisk
(v1.8)](http://www.asterisk.org/) included a module to connect directly
to your GV number, giving you a free phone number, potentially for
life!  
  
There is already an ["everything included"
installer](http://nerdvittles.com/?p=705) available from NerdVittles,
but it has WAY more than you need if all you want is to connect your GV
number to a regular phone at home. To that end, I'll outline the steps I
followed to get a regular wireless phone connected to my Google Voice
number for making and receiving calls.  
**[UPDATE: The NerdVittles package is pretty much awesome.  It's got a
lot included, but it's all good.  The install is fast and painless, and
a little easier than what I outlined...  I switched to this one at home,
and suggest you do the same if you want to tie your Google Voice account
to a phone.]**  
  
You'll need a spare computer (but it does NOT need to be powerful or
have a ton of RAM or disk space - you might even be able to use a free
Amazon EC2 instance). You'll also need a SIP/POTS adapter, [like this
\$30 unit from
Grandstream](http://www.voipsupply.com/grandstream-gs-286). You'll also
need to be nerdy enough to have installed Linux once or twice before,
though you do NOT need to be an expert to do this... What follows are
all the steps you'll need to set up and configure an absolutely minimal
Asterisk installation.  
  
Install [CentOS 5.5](http://centos.org/) - choose the "Server" install.
After installation, run the following commands:  

>     yum -y updateyum -y install bison bison-devel ncurses ncurses-devel \zlib zlib-devel openssl openssl-devel gnutls-devel \subversion automake gcc gcc-c++ libtool gnutls \gnutls-devel texinfo kernel-devel

Download Iksemel from <http://code.google.com/p/iksemel/>, expand in
/usr/src, then run:  
<span style="font-family: 'courier new';"></span>  

>     ./configure --prefix=/usrmakemake check [be sure all tests pass!]make install

[Download the newest version of
Asterisk](http://downloads.asterisk.org/pub/telephony/asterisk/), expand
in /usr/src, then run:  

>     ./configure --prefix=/usrmakemake install

Add the following files in /etc/asterisk:  
logger.conf:  

>     [logfiles]console = warning,notice,error

modules.conf:  

>     [modules]autoload=yes

gtalk.conf:  

>     [general]context=google-in ; Context to dump call intoallowguest=yes[guest]disallow=allallow=ulaw[myaccount-gtalk]username=[YOUR GMAIL USER]@gmail.comdisallow=allallow=ulawcontext=google-inconnection=gtalkconnection

jabber.conf:  
<span style="font-size: 85%;"><span
style="font-family: 'courier new';"></span></span>  

>     [gtalkconnection] type=clientserverhost=talk.google.comusername=[YOUR GMAIL USER]@gmail.com/asterisksecret=[YOU GMAIL PASSWORD]statusmessage="I am asterisk"status=xaway

sip.conf:  
<span style="font-family: 'courier new'; font-size: 85%;"></span>  

>     [general]port=5060bindaddr=0.0.0.0disallow=allallow=ulaw[101]type=peerhost=dynamicsecret=asecretpasswordcontext=users

extensions.conf:  

>     [default]exten => s,1,Set(CALLERID(name)=${DB(cidname/${CALLERID(num)})})exten => s,n,Dial(SIP/101,10)exten => s,n, Hangupexten => 101, 1, Dial(SIP/101, 10)[google-in]exten => [YOUR GMAIL USER]@gmail.com, 1, Dial(SIP/101, 180, D(:1))[stations]exten => 101,1,Macro(phone); * Route the call using the google voice bridge[gv-outbound];append an area code if necessaryexten => _NXXXXXX,1,Set(CALLERID(dnid)=1323${CALLERID(dnid)})exten => _NXXXXXX,n,Goto(1323${EXTEN},1);append a 1 if necessaryexten => _NXXNXXXXXX,1,Set(CALLERID(dnid)=1${CALLERID(dnid)})exten => _NXXNXXXXXX,n,Goto(1${EXTEN},1);do our real dialingexten => _1NXXNXXXXXX,1,Dial(Gtalk/gtalkconnection/${EXTEN}@voice.google.com)exten => _+1NXXNXXXXXX,1,Dial(Gtalk/gtalkconnectionEXTEN}@voice.google.com)[users]include => stationsinclude => gv-outbound

You can now start up asterisk (in a sort of test mode) with
"/usr/sbin/asterisk -vvvgc". Keep that console connection open until
you've confirmed everything works as planned. (When it's all good,
you'll find appropriate init scrips in the contrib directory of the
asterisk source.) Check to see if you're connected to your Google Voice
account with "jabber show connected" from the asterisk console. If
you're not connected, then something went wrong...  
  
The only other thing you have to do at this point is configure your SIP
client (whether it's a soft-phone or an adapter like the Grandstream
device I linked to above.) Then try dialing out, and call your GV number
from another phone to test the in-bound calling!  
  
You can do a LOT more with Asterisk, but if you just want to get your GV
number linked to a phone at home, this is all you need to do!  
  
  
  
<span style="font-size: 85%;">Thanks to these folks for their
information, what they had to share helped me sort this all out:  
</span>  

-   <span
    style="font-size: 85%;">http://supermario-world.blogspot.com/2010/11/asterisk-18-and-native-google-voice.html</span>
-   <span
    style="font-size: 85%;">http://www.voip-info.org/wiki/view/Asterisk+Speaks+with+Google+Talk</span>
-   <span
    style="font-size: 85%;">http://www.asteriskguru.com/tutorials/asterisk\_installation\_compilation\_centos.html</span>
-   <span
    style="font-size: 85%;">http://jeremy-mcnamara.com/asterisk/how-to-configure-asterisk/</span>
-   <span
    style="font-size: 85%;">http://blog.polybeacon.com/2010/10/17/asterisk-1-8-and-google-voice/</span>

</p>

