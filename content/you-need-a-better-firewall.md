Title: You need a better firewall
Date: 2014-02-03 19:47
Author: Christopher Aedo
Tags:  thingsilike, necessary skills, voip, improvement, be better
Slug: you-need-a-better-firewall

![picture](/images/pfsense-logo.png)

A long long time ago I had Verizon FiOS installed at my home, and with
it they supplied an Actiontec router and wireless access point. It
worked reasonably well, but it needed a power cycle every now and then
and any custom configuration was difficult. I could never get it to
prioritize traffic properly, and to be honest I just plain missed
[pfSense](http://www.pfsense.org/). I expected one day I would get
around to replacing the router/WAP they supplied, and I finally did at
the end of December.  
  
![picture](/images/fbsd.png)

The real question was what do I replace the default gear with?  One of
the guys I worked with many years ago had suggested we try
[pfSense](http://www.pfsense.org/) for the office and after spending a
few days testing it, I was in love. It's based on
[FreeBSD](http://www.freebsd.org/), uses [OpenBSD's pf packet
filter](http://openbsd.org/faq/pf/), and provides an excellent web
interface. It can be run from a USB memory stick, run from a live-cd, or
installed on just about any common hardware that can run
[FreeBSD](http://www.freebsd.org/).  After running multiple pfSense
firewalls for years, I can say it's the most reliable and easiest to use
that I've experienced.  
  
Alternatively, at least for home, I could just buy a Netgear WAP.
 They're not half bad, [except for the
backdoors](https://github.com/elvanderb/TCP-32764) and generally poor
performance if you have more than a small handful of clients connecting
to them.  
  
An aside: on occasion the weak security of these commodity WAPs can be a
benefit as long as you're not using it on your own network.  Once on
vacation, the hotel we stayed in provided free wifi.  Unfortunately they
had far more users than their access point could handle.  They "solved"
this problem by adding additional access points and suggesting that when
visitors had trouble with "hotel-wifi-1" they try "hotel-wifi-2", and so
on.  There were three or four APs, and once you could get connected to
one, your session would work for about two minutes.  Suspecting they
were just getting overwhelmed with connections, I thought I would check
to see if I could help by adjusting the configuration.  These particular
AP's had never been updated, and there was a known vulnerability I could
exploit.  By visiting a specific URL (without authenticating), the unit
would dump it's config file which included the admin password, in
plaintext.  Once I had that, whenever I needed the internet for a few
minutes I would just add my mac address to one of the units and limit
access to authorized devices only.  Suddenly the WAP performed
beautifully!  When I was done, I returned the config to default and the
other patrons could continue using it for a few minutes at a time.  
  
If you're sticking with a commodity wireless AP/firewall, at least try
to install [OpenWRT](https://en.wikipedia.org/wiki/OpenWrt) on it,
though you might be starved for ram and cpu if you want to do traffic
shaping and manage VPN connections.  But at least you'll have superior
security, and a much more usable interface.  
  
In general though I still maintain you're better off spinning up your
own box using pfSense.  It's absolutely "enterprise grade" stuff.  It
will easily support OpenVPN for site-to-site tunnels, or just for your
own machine (so when you're on an unfriendly/unknown network you can
encrypt all your traffic over that tunnel back to your home, and out to
the 'net from there!)  If you need redundancy, it's trivial to set up a
second pfSense box and using CARP with shared state tables, you can have
invisible failover.  
  
Are you sold on [pfSense](http://www.pfsense.org/) yet?  You can
certainly spin it up on just about any piece of commodity equipment you
may have lying around, but the power consumption could be a little silly
for home if you happened to use an older PC for this.  Unless you have
an unusually beefy link at home you will not need a TON of nic or cpu
horsepower - a small fan-less computer is likely your best best.  If you
fear that will not cut it, there are some [good sizing recommendations
on the pfSense](http://www.pfsense.org/hardware/index.html#sizing) site
to help guide you.  
  
Assuming you're like me (and I know I am!) the best hardware choice is
an ALIX.  You can [buy ALIX kits from the US through
Netgate](http://store.netgate.com/Search.aspx?k=pfsense).  You can also
[buy directly from the manufacturer
(PCEngines)](http://www.pcengines.ch/).  Using this hardware I had three
NICs plus a Wistron wifi card, all in a tiny aluminum enclosure that
sips tiny amounts of power.  Here's exactly what I ordered from
PCEngines for about $175 including fast shipping from Switzerland:  

* alix2d3 ALIX.2D3 system board (500mhz AMD Geode LX CPU)**
* case1d2redu Enclosure 3 LAN, red, USB
* ac12vus AC adapter 12V US plug for IT equipment
* cf2slc CompactFlash card 2GB SLC
* (2) pigsma Pigtail cable I-PEX -\> reverse SMA
* (2) antsmadb Antenna reverse SMA dual band
* dnma92 Wistron DNMA92 miniPCI card
* (2) Antennas (you will need them, I already had some)
  
Since [there are already a ton of how-to's on setting up pfSense on an
ALIX](https://encrypted.google.com/search?hl=en&q=set%20up%20pfsense%20on%20ALIX),
I won't go through my steps.  It was easy to do though, and I'm superbly
satisfied.  The box is now directly connected to the ONT ([tips on that
here](http://www.dd-wrt.com/wiki/index.php/Verizon_FiOS_-_Using_Your_Own_Router)),
and my network at home is healthier than it's ever been.  So what are
you waiting for?  Get on it!  (Shoot me an email if you get stuck!)
