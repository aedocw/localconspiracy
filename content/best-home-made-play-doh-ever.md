Title: Best home-made play-doh EVER
Date: 2012-05-26 23:18
Author: Christopher Aedo
Tags:  thingsilike, random, necessary skills, improvement, be better
Slug: best-home-made-play-doh-ever

If you have kids, you probably love playing with them and making things
together.  One of the best things you can do for them is to encourage
imaginative play, and few things enable imagination in a young mind
better than a nice lump of play-doh.  
  
After experimenting with several different variations, my wife found
this simple yet excellent play-doh recipe.  It is the best version of
home made play-doh by a long shot.  Have fun with it!  
  
In a medium pot, combine and stir the following:  

* 1 cup warm water
* 1 cup white flour
* 1/4 cup salt
* 2 tsp cream of tartar
* 1 tbsp vegetable oil
* 1 packet of kool aid (optional, adds color and a nice scent too)
 
Now heat pot over low-medium heat while stirring constantly.  Eventually
the dough will combine into a ball.  Remove from heat, and make sure
it's all well combined.  Flatten the ball out on a piece of wax paper to
cool, and start playing whenever you think it's ready for those little
hands!  
  
Be sure to store in an air-tight bag or container when done, to prevent
drying out.  If a batch does dry a bit, it can be revived by kneading in
a little water and a little vegetable oil.  
  
There you have it, the best home-made play-doh EVER!
