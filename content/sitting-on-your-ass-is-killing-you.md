Title: Sitting on your ass is killing you
Date: 2012-10-21 19:36
Author: Christopher Aedo
Tags:  downfall of humanity, beating a dead horse, improvement, exercise, fitness, be better
Slug: sitting-on-your-ass-is-killing-you

  
This is not the first time I've said [don't watch
TV](/2012/09/no-news-is-good-news.html),
or [don't sit at
work](/2010/08/get-up-stand-up.html).  But
[this new article from NY
times](http://well.blogs.nytimes.com/2012/10/17/get-up-get-out-dont-sit/)
references some new studies that make the dangers of lethargy all the
more vivid.  

> "Every single hour of television watched after the age of 25 reduces
> the viewer’s life expectancy by 21.8 minutes.  By comparison, smoking
> a single cigarette reduces life expectancy by about 11 minutes, the
> authors said."

Of course that prompts me to quote again from one of my favorite old
articles ["Turn It Off" by Craig
Russel](http://www.strike-the-root.com/3/russell/russell4.html) (read
the whole article, it'll only take you a minute and it's well worth
it!)  

> "Each of us only has so much time allotted to him. We glow but a short
> while upon this earth, and then the flame dies. Why would we spend
> even a second of it in the fatal radiation of an infernal device that
> promises 'entertainment,' that promises 'enlightenment,' but delivers
> only weak facsimiles?"

  

</p>

