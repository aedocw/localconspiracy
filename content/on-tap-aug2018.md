Title: Whats On Tap, August 2018
Date: 2018-08-10 13:00
Author: Christopher Aedo
Tags: beer, kitchentaps
Slug: on-tap-aug2018

![image](/images/on-tap-aug2018.jpg "four beer taps")

I [promised to do this whenever something on tap
changed](/2018/02/on-tap-feb2018.html) but I completely failed to
stay on top of that.  Instead maybe I'll just do it when ALL the
taps have rotated, as I am doing this time!

The Saison is tasty, but came out a little higher gravity than
intended.  That was the result of a (lucky? unfortunate?)
confluence of a higher than expected mash plus really happy
yeast.

The Pale was a huge hit, perhaps the best thing on tap this time
around.  This will probably be the next thing I brew, just to be
sure it's on tap at all times.

The ESB was a fun one, brewed in a 10g batch with a friend so he
could take a keg with him to an event.  It turned out really good
and is probably the most easy drinking thing on tap right now.

The last one (C.J.) is my "Conceited Jerk" recipe, which is a
clone of Arrogant Bastard that I've been making changes to for
ten years.  On the taste front it's pretty close, but it's still
too dark.  This time around I got a lot closer though, except it
came out higher ABV than the real thing.  Still tastes great and
really hits the spot sometimes.

