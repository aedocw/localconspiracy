Title: Helpful tools for organization
Date: 2022-06-03 13:40
Author: Christopher Aedo
Tags: 
Slug: staying-organized

Lately I've been feeling really good about the tools I'm using to keep my
work (and occasionally personal) information and tasks organized. As both a
reminder to myself if I ever need to recreate everything from scratch, and
something I can share with others who might find this useful, I thought I
should write about it.

First off is using a calendar religiously. We use gmail at work, and I try
to be really good about putting in ALL appointments and time-specific
reminders, not just work meetings. For instance on the home front, I have a
recurring entry every year that reminds me to call the roof cleaner (we
live in the PNW and it's pretty important to keep your roof healthy if you
don't want it to need an early replacement). I'm also careful to put in the
location for any appointments I've got so when I get in the car the
directions will automatically pop up on CarPlay when the time for the
appointment nears.

The second most useful tool I've been using for a while is
[TaskWarrior](https://taskwarrior.org/). It has become absolutely
invaluable for me both for keeping track of the things I need to do, and
for keeping a record of the things I've been up to. To make it easier I've
got a few aliases set up:

~~~~
alias ta="task add"
alias taw="task add +work "
alias td="task done"
alias tl="task list"
alias tlw="clear;task list +work"
alias ts="task sync"
~~~~

I tag anything work-specific with "+work" so my "tlw" alias shows me all
the outstanding work tasks I have. I don't even use a tiny fraction of the
capabilities of this tool to be honest. You can use it as a full featured
project management tool with sub-tasks, dependencies, due dates,
prioritization, etc. For me I just use it as a place to hold relatively
short term reminders like "Check in with Joe re repo access" or
"Follow up with HR re bonuses".

When I want to remind myself of what I've done in the past I just use "task
completed +work" and I get a detailed list of what I marked as done and
when it was completed.

Since I occasionally switch between two computers, I wanted to keep the
task list synchronized, that's the alias "task sync". For that I set up a
[Taskserver](https://taskwarrior.org/docs/taskserver/why.html) with
docker-compose. The [Taskserver Setup
Guide](https://gothenburgbitfactory.github.io/taskserver-setup/) was super
helpful for this.

The last thing I've been doing that has been incredibly valuable is making
it really easy to keep notes and search my notes for keywords or phrases. I
know there are a TON of fancy approaches to this, for instance SaS options
like Evernote and several open source self-hosted note-taking apps that are
similar. The thing is I just don't need to create or check notes from my
phone or an iPad or some random computer. I just want to be able to quickly
jump into a note from my terminal when I'm in a meeting or need to write
out something too long for a TaskWarrior task. To that end I've got the
following functions in my .zshrc:

~~~~
function notegitpush {
	pushd $HOME/Documents/notes
	git pull
	git add *
	git commit -m "Autocommit due to edit of $FILE"
	#do lame check to see if we are online before git push
	if ping -c 1 gitlab.com &>/dev/null
	then git push origin main
	else echo "Offline, not pushing changes up"
	fi
	popd
}

function notegitpull {
	pushd $HOME/Documents/notes
	#do lame check to see if we are online before git pull
	if ping -c 1 gitlab.com &>/dev/null
	then git pull
	else echo "Offline, not pulling changes"
	fi
	popd
}

function note {
	DATE=`date +"%Y%m%d"`
	TS=`date +"%Y%m%d %H:%M:%S"`
	FILE=$HOME/Documents/notes/$DATE.txt
	notegitpull
	echo $TS >> $FILE
	vi $FILE
	notegitpush
}

function search {
	egrep -i $1 ~/Documents/notes/*
	task list +work | egrep -i $1
	echo "(e)dit with vim, (l)f, or (q): "
	read -k x
	if [[ $x == 'e' ]]
	then
	  vi `egrep -il $1 ~/Documents/notes/*`
	elif [[ $x == 'l' ]]
	then
	  lf ~/Documents/notes
	else
	fi
}
~~~~

Typing "note" jumps me into a new note with the filename of today's date,
with a date and time-stamp line inserted at the bottom. Then I can start
typing immediately below that and take down whatever I need to. When I exit
the file it's automatically committed to git and pushed to a private repo.

The alias "search" lets me search for a word or phrase and see which files
or open tasks contain what I'm looking for. That's been really helpful so
many times when I have a vague recollection of something and I want to find
more details.

Using these three tools (google calendar, TaskWarrior and a simple function
for creating notes with vim) has made it really easy for me to stay on top
of the things I need to do, and occasionally look back and see what I've
done. If you ever have trouble staying organized, give it a try!

