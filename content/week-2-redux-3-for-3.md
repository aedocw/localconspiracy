Title: Week 2 (redux) - 3 for 3
Date: 2010-04-24 21:39
Author: Christopher
Tags: 
Slug: week-2-redux-3-for-3

[Last week](/2010/04/excuses-excuses.html)
I got a cold that totally kicked my ass. I was only able to ride on
Monday, and the week went downhill for me after that - so I say this
week is a do-over for last week and will stand as my official "week
2"...  
  
It's definitely getting easier to get out the door for the ride. I know
the route well enough to never have to think about it, and have figured
out the quickest way through a few tricky intersections. Not having to
pay attention to the route is nice, I get to space out and think about
other stuff and before I know it I'm home!  
  
Monday was tough this week - it was cold (at least cold for me, low
50's). Thursday was cold too but not quite as bad. By Friday the weather
had gotten beautiful again, that helps a lot.  
  
With two weeks left I think this is going to be an easy habit to get
into. 210 miles commuted so far. I think my next goal will be to commute
(by bike) 1000 miles. How many miles should I ride before I treat myself
to a new bike :D ?  

-   [To work (4/19)](http://connect.garmin.com/activity/30640454)
-   Back home (4/19) - no data :( Forgot to charge the garmin so battery
    only lasted for the ride in
-   [To work (4/22)](http://connect.garmin.com/activity/30936692)
-   [Back home (4/22)](http://connect.garmin.com/activity/30936699)
-   [To work (4/23)](http://connect.garmin.com/activity/31080482)
-   [Back home (4/23)](http://connect.garmin.com/activity/31080493)

</p>

