Title: IRC, the secret to open source success
Date: 2015-12-19 14:03
Author: Christopher Aedo
Tags: 
Slug: irc-secret-success

[Yes, this is another IRC post - this one was written for [IBM's
OpenTech blog](https://developer.ibm.com/opentech/2015/12/20/irc-the-secret-to-success-in-open-source/
), but I thought I'd add it here since my blog tends to be thin on
updates ;) Anyway, hope you enjoy it!]

Effective communication is key when collaborating on an open source
project and being reachable is the first step.  For many open source
projects the two key communication channels used are email and IRC.
Email tends to be used for longer conversations on topics that might
span weeks or even months of back and forth, and IRC is the medium of
choice when you need to gather information or come to a consensus
quickly.  Everybody has an email address, but not nearly as many
people manage to stay connected on IRC. Being available on IRC makes a
huge difference if you want to be a truly effective member of the open
source community.

The best thing about IRC is that it can really bring a distributed
community together. For instance, with
[OpenStack](http://openstack.org) many projects have [their own IRC
channel](https://wiki.openstack.org/wiki/IRC) where those involved
hang out.  Additionally there are a few other channels that span
multiple projects (like #openstack-infra) where you can find a huge
number of active contributors.  Hanging out on those channels is a
great way to get to know the people involved and is essential if you
want to receive or provide help quickly.  Hopping on one of these IRC
channels can feel like wandering into a room filled with a bunch of
great friends.

If you are interested in becoming an OpenStack contributor having a
persistent IRC presence might be one of the most important secrets to
success.  Anyone who spends time in one of the more active project
channels will immediately see the value of synchronous communication
here where problems are sometimes solved in minutes.

The challenge with a synchronous communication channel like IRC is
that not everyone is awake and online at the same time.  If you're
using an IRC client that goes offline when you aren't around you are
often missing part of the conversation.  It also makes it difficult
for people to find you if you're going on and off-line several times a
day.  This issue can be solved by running your IRC client (like
[irssi](https://irssi.org/)) in a screen or tmux session on a machine
that's always online and then connecting to that machine as needed.
The biggest problem with this approach is that it makes it hard to
catch a notification if you're not connected to the remote session.

To solve this some people configure an IRC bouncer like
[ZNC](http://wiki.znc.in/ZNC).  This is essentially an IRC proxy,
which buffers messages when you're not connected to it and plays the
messages back when you return.  With most bouncers you can configure a
notification to alert you via email if your IRC nickname is mentioned
while you're offline.  If you have a bouncer set up, you can then
connect to it with any IRC client that runs on your platform of
choice.  Sean Dague wrote a really good doc on [his IRC proxy
setup](https://dague.net/2014/09/13/my-irc-proxy-setup/), and even
included a puppet manifest to automate the setup.  This works
reasonably well, but can get pretty complicated when you want to
connect from multiple IRC clients at the same time, like your desktop
and your phone for instance.

What if you also have cause to use [Slack](http://slack.com)? Many
teams are using Slack as essentially a hosted IRC server and client.
If you are anything like me, you've already been invited to join
several different Slack orgs, which means in addition to paying
attention to a dozen (or a few dozen) IRC channels you also have to
keep several browser tabs open for all the Slacks.

If you are looking for some way to unify all these messaging platforms
(as I was), I've found the perfect solution -
[WeeChat](https://weechat.org/), an IRC client with a built in relay
that makes it incredibly easy to stay connected in a terminal session,
a web app and your phone, all at the same time.  You get a consistent
view across all your devices, and can catch up on any conversation by
scrolling back up.  If you run the Android client you'll get a
notification on your phone when someone mentions your nick.  If you
use Glowing-Bear web client, you'll get a notification on your desktop
too!  It's lightweight and easy to install, and has an excellent
modular architecture with [a great many scripts and plugins
available](https://weechat.org/scripts/) including one called
[WeeSlack](https://github.com/rawdigits/wee-slack) that allows you to
bring several slack organizations and all their channels under one
roof.

![weechat terminal scrrenshot](/images/weechat-term.png)

Installing [WeeChat](https://weechat.org/) is very straightforward,
though the steps to take depend on your platform of choice.  I decided
to install [WeeChat](https://weechat.org/) on an Ubuntu VM running on
a public OpenStack cloud - installation was as simple as typing "sudo
apt-get install weechat".  Once installed, [their
docs](https://weechat.org/doc/) are excellent.  DON'T FORGET to start
WeeChat in [a screen session](http://www.gnu.org/software/screen/) so
you can disconnect without shutting it down!

Once WeeChat is running you can enable an encrypted relay and get
connected from the [WeeChat Android
Client](https://play.google.com/store/apps/details?id=com.ubergeek42.WeechatAndroid&amp;hl=en).
It might not be your first choice (at least for me, I do not type
incredibly fast or error-free on my phone!) but it sure does come in
handy when you are following along with an ongoing conversation.  It's
also great when someone pings you, and you can quickly respond with
"away from the computer, but I'll ping you when I return in half an
hour!"

![weechat android client screenshot](/images/weechat-android.png)

You can also get connected from your browser with [Glowing
Bear](https://github.com/glowing-bear/glowing-bear), which is my
personal favorite.  I'll get a quick notification on the desktop if
I'm mentioned somewhere and all the IRC channels and Slack
organization I'm connected to are in one convenient tab.

![Glowing-bear.org](/images/gb-screen.png)

Details on setting up the encrypted relay can be found in the WeeChat
docs, or even easier, by visiting
[http://www.glowing-bear.org](http://www.glowing-bear.org) and
clicking "Encryption instructions".  It will take less than a minute,
and SHOULD be your connection method of choice.

Along the way you'll want to get connected to
[freenode.net](http://freenode.net). When you do, be sure to pick a
nickname you like, and [don't forget to register
it!](https://freenode.net/faq.shtml#userregistration)!

I really believe being available on IRC can make a tremendous
difference if you are involved in the open source community, and my
personal experience definitely supports this.  There are several
different ways to achieve a persistent presence on IRC but hopefully
you'll agree WeeChat is the best option around.  If you happen to get
connected to a freenode server please say hello, it's as easy as
"/msg docaedo Hi there!"
