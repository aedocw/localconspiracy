Title: Inspired by our founders
Date: 2012-08-14 04:40
Author: Christopher
Tags:  homebrew, beer, necessary skills, improvement, health, be better
Slug: inspired-by-our-founders

> "AMERICA has a proud history of drinking on the job. Craftsmen who built
> the first government buildings in the 17th century were sometimes paid
> in brandy. The 19th-century railroaders who laid the foundations of
> modern America were notoriously thirsty."  
  
[The Economist](http://www.economist.com/node/21560265)
  
Obviously for the sake of our country I need to make a kegerator for the
office and start brewing more often!

