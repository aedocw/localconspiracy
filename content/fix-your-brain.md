Title: Fix your brain!
Date: 2012-04-29 15:23
Author: Christopher
Tags:  improvement, be better
Slug: fix-your-brain

  
Can you make yourself smarter?  There's a lot of evidence you can, but
it takes a little effort on your part (15 minutes a day is enough to
yield pretty significant improvements.)  You can no doubt find something
each day that you're spending 15 minutes (or more on) that you can drop,
in favor of fixing your brain and making your life a little easier :)  
  
"[...] young adults who practiced a stripped-down, less cartoonish
version of the game also showed improvement in a fundamental cognitive
ability known as “fluid” intelligence: the capacity to solve novel
problems, to learn, to reason, to see connections and to get to the
bottom of things. The implication was that playing the game literally
makes people smarter."  
[Can you make yourself
smarter?](http://www.nytimes.com/2012/04/22/magazine/can-you-make-yourself-smarter.html?_r=2&hpw=&pagewanted=all#)  
  
If you have an iPhone, there's a [great FREE Dual N-Back game
here](http://itunes.apple.com/us/app/iq-boost/id286574399?mt=8).  
  
If you want to try it on your computer, [the best one out there is also
free](http://brainworkshop.sourceforge.net/).  
  

