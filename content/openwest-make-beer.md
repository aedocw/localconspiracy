Title: Making Beer with Linux, Python and a Raspberry Pi
Date: 2017-07-13 12:00
Author: Christopher Aedo
Tags: beer, speaking, open source, iot, python, pi
Slug: openwest-make-beer

![taps](/images/taps.png)

I was lucky to have a talk about brewing beer with
Linux, Python and a Respberry Pi chosen for [OpenWest
2017](http://openwest.org).  As part of that talk I wanted
to put up a quick post with the slides from the talk and
links to some of the resources I used.  It's also worth
taking a look at a previous post about [my electric
brewery](/2016/12/electric-brewery.html) as I include some
details and links there as well.

In preparation for the talk [I wrote about the system for
opensource.com](https://opensource.com/article/17/7/brewing-beer-python-and-raspberry-pi).
That turned out to be really popular, and was then
[mentioned on
Hackaday](http://hackaday.com/2017/07/09/making-a-small-scale-brewery-with-a-raspberry-pi-and-python/)
and
[raspberrypi.org](https://www.raspberrypi.org/blog/homebrew-beer-brewing-pi/)
as well.  It's probably worth reminding folks to follow me
on twitter [@docaedo](https://twitter.com/docaedo/) if you
want any updates on the next things I'll be doing with the
system.  It's also a pretty convenient way to reach me if
you have questions :)

Resources:

* [Slides](/images/beer-linux-pi.pdf)
* [Hosehead](https://brewtronix.com)
* [The Electric Brewery](http://www.theelectricbrewery.com/)
* [Control Panel on the Cheap](http://www.instructables.com/id/Electric-Brewery-Control-Panel-on-the-Cheap/)
* [CraftBeerPi Github](https://github.com/Manuel83/craftbeerpi/)
* [CraftBeerPi](http://craftbeerpi.com)

