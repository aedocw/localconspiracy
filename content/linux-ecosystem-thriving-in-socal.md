Title: Linux ecosystem thriving in SoCal!
Date: 2014-02-22 20:24
Author: Christopher Aedo
Tags: 
Slug: linux-ecosystem-thriving-in-socal

![scale12x](/images/scale_12x_dodecahedron.png)

I'm heading back to the [12th Annual Southern California Linux
Expo](http://www.socallinuxexpo.org/scale12x) and really looking forward
to another round of great sessions and mingling with the other nerds I
run into at the meetups.  
  
This conference has brought something to mind that I'd considered in
passing previously, but now I'm thinking much more seriously about it.
 That would be the role of "part time CTO".  
  
In the last six months I've been helping a few small companies fill gaps
with their sysadmin needs or devops/techops leadership.  It's surprising
how quickly we've been able to shore up their networks, straighten out
their disaster recovery plans, address critical security concerns, etc.
 Having 20+ years of experience under my belt makes much of this stuff
seem crazily obvious to me - it's easy to forget there was a time when I
didn't know everything I know today :)  
  
For the vast majority of small and growing companies (established shops
and startups alike), it can be way less expensive to have me or someone
like me on staff part-time.  You can get all the value of a veteran
CIO/CTO at a fraction of the expense.  
  
I have the spare cycles right now to expand what I have to offer, so if
you could use someone with deep expertise in TechOps, DevOps, cloud and
scalability, let's talk - contact me at <doc@aedo.net>!
