Title: From Glance to Boot-From-Volume in 2 minutes flat
Date: 2013-05-10 23:05
Author: Christopher
Tags:  openstack, scalability, kvm, glance
Slug: from-glance-to-boot-from-volume-in-2-minutes-flat

Here’s
a quick list of the steps necessary to pull an image or snapshot from
[Glance](http://www.openstack.org/projects/image-service/), copy it to a
[Cinder](http://docs.openstack.org/developer/cinder/) volume, and then
boot from that volume.  In an OpenStack environment using local storage
for VMs this is a convenient way to run a VM with a need for an
unusually large root volume.  It also makes VM migration significantly
quicker and easier (vs. KVM block-migration for instance).  Used
judiciously, this can also help you get the most out of all your
available resources by combining both local and remote instance
storage.

The following steps were performed inside an
[OpenStack](http://openstack.org/) environment on an Ubuntu 12.04
VM.

1.  Get the latest Glance and Nova clients:
> aptitude install python-pip  
> pip install python-glanceclient  
> pip install python-novaclient  

2.  Find the image (or snapshot) you want:
> glance indexID                                   Name                           Disk Format    
> [...]------------------------------------ ------------------------------ --------------------    
> [...]3747647c-e43e-4f48-a5b5-59163a45db17 Ubuntu 12.04.1                 qcow2                   
> [...]    

3. Download the image to a local file:
> glance image-download 3747647c-e43e-4f48-a5b5-59163a45db17 --file ubuntu1204.img

4. Provision a volume and attach to this VM:
> nova volume-create --display-name MyVMVolume 20  
> +---------------------+--------------------------------------+  
> | Property            | Value                                |  
> +---------------------+--------------------------------------+  
> | status              | creating                             |  
> | display_name        | MyVMVolume                           |  
> | attachments         | []                                   |  
> | availability_zone   | nova                                 |  
> | created_at          | 2013-05-08T02:27:51.611264           |  
> | display_description | None                                 |  
> | volume_type         | None                                 |  
> | snapshot_id         | None                                 |  
> | size                | 20                                   |  
> | id                  | 733a336a-0f68-48f5-91c7-f4f8f4369a63 |  
> | metadata            | {}                                   |  
> +---------------------+--------------------------------------+  
>   
> nova list  
> +--------------------------------------+--------------------------+--------+------------------------------------+  
> | ID                                   | Name                     | Status | Networks                           |  
> +--------------------------------------+--------------------------+--------+------------------------------------+  
> | 48603fde-05a4-4bf9-bd3d-be947f97b29d | cwa1                     | ACTIVE | novanetwork=10.12.0.7, 10.50.12.67 |  
> +--------------------------------------+--------------------------+--------+------------------------------------+  
>   
> nova volume-attach 48603fde-05a4-4bf9-bd3d-be947f97b29d 733a336a-0f68-48f5-91c7-f4f8f4369a63 /dev/vdc  
> +----------+--------------------------------------+  
> | Property | Value                                |  
> +----------+--------------------------------------+  
> | device   | /dev/vdc                             |  
> | serverId | 48603fde-05a4-4bf9-bd3d-be947f97b29d |  
> | id       | 733a336a-0f68-48f5-91c7-f4f8f4369a63 |  
> | volumeId | 733a336a-0f68-48f5-91c7-f4f8f4369a63 |  
> +----------+--------------------------------------+  

5. Convert the image from qcow2 to raw and send the output to the attached
volume:
> qemu-img convert -f qcow2 -O raw ubuntu1204.img /dev/vdc

6. Detach the volume
> nova volume-detach 48603fde-05a4-4bf9-bd3d-be947f97b29d 733a336a-0f68-48f5-91c7-f4f8f4369a63

7. Launch the VM from that volume (NOTE: you have to specify an image,
but nova won’t actually USE that image, your VM will boot from the
volume you just created)
> nova boot --image=3747647c-e43e-4f48-a5b5-59163a45db17 --flavor 2 \--block_device_mapping vda=733a336a-0f68-48f5-91c7-f4f8f4369a63:::0 \--key_name caedo VM-on-Volume  
> +-------------------------------------+--------------------------------------+  
> | Property                            | Value                                |  
> +-------------------------------------+--------------------------------------+  
> | status                              | BUILD                                |  
> | updated                             | 2013-05-08T02:38:56Z                 |  
> | OS-EXT-STS:task_state               | scheduling                           |  
> | OS-EXT-SRV-ATTR:host                | None                                 |  
> | key_name                            | caedo                                |  
> | image                               | Ubuntu 12.04.1                       |  
> | hostId                              |                                      |  
> | OS-EXT-STS:vm_state                 | building                             |  
> | OS-EXT-SRV-ATTR:instance_name       | instance-00000066                    |  
> | OS-EXT-SRV-ATTR:hypervisor_hostname | None                                 |  
> | flavor                              | m1.small                             |  
> | id                                  | 783f3f13-a595-4aed-affa-012cedbe7897 |  
> | security_groups                     | [{u'name': u'default'}]              |  
> | user_id                             | dcb614827c79456d8f95150b393632b3     |  
> | name                                | VM-on-Volume                         |  
> | adminPass                           | y2PbhKDPYWjp                         |  
> | tenant_id                           | e89935d735304e79bc39a8835b42d20e     |  
> | created                             | 2013-05-08T02:38:56Z                 |  
> | OS-DCF:diskConfig                   | MANUAL                               |  
> | accessIPv4                          |                                      |  
> | accessIPv6                          |                                      |  
> | progress                            | 0                                    |  
> | OS-EXT-STS:power_state              | 0                                    |  
> | metadata                            | {}                                   |  
> | config_drive                        |                                      |  
> +-------------------------------------+--------------------------------------+  

And that’s it - now your VM base image is running from a volume!

