Title: Your to-do list
Date: 2012-04-27 15:34
Author: Christopher
Tags:  necessary skills, improvement
Slug: your-to-do-list

It's easy to lose sight of your dreams when you're neck-deep in the day
to day stuff you need to do just to survive.  If you are at least taking
time to make lists of things you need to get done, and prioritizing them
at that, Paul Graham makes a great suggestion for what should be at the
top of your list every time:  

> "I would like to avoid making these mistakes. But how do you avoid
> mistakes you make by default? Ideally you transform your life so it
> has other defaults. But it may not be possible to do that completely.
> As long as these mistakes happen by default, you probably have to be
> reminded not to make them. So I inverted the 5 regrets, yielding a
> list of 5 commands:  
> Don't ignore your dreams; don't work too much; say what you think;
> cultivate friendships; be happy."

(From [Paul Graham's to-do list](http://paulgraham.com/todo.html),
commenting on ["Regrets of the
Dying"](http://www.inspirationandchai.com/Regrets-of-the-Dying.html))

