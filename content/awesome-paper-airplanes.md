Title: Awesome Paper Airplanes
Date: 2014-02-01 18:08
Author: Christopher Aedo
Tags:  thingsilike, random
Slug: awesome-paper-airplanes

![plane](/images/IMG_20140127_130454235.jpg)

When I was growing up, I loved making paper airplanes.  I also used to
spend a lot of time at [Chicago's Museum of Science and
Industry](http://www.msichicago.org/).  One time when I was there I saw
a kit full of amazing looking plans for planes.  They were ["Whitewings"
model kits](http://whitewings.com/), and I absolutely loved them.  The
plans for the plane were printed on card-stock, and you were meant to
very carefully cut out all the pieces, then glue them together in the
right order.  Ultimately you would end up with a fuselage that was
several pieces glued together, then a wing (two or three sheets thick)
and horizontal stabilizer.  
  
![plane](/images/plane-plans.png)

A few years ago I [ordered a new kit from Amazon](http://goo.gl/bXAc5y)
when we had our first son, thinking I would build them and we would fly
them together one day.  Fast forward five years, and the kit was still
sitting on the bookshelf next to what remained from the first kit I got
as a child.  I would occasionally notice them and think "man, I should
build some of those!"  Then I would remember how long the cutting-out
part took, and the box would stay on the shelf to be saved for "some
other day".  
  
This year for christmas I got my wife a computer-driven die-cutter.
 Basically it's a vector printer except with a little xacto blade
instead of a print head.  It's the [Silhouette
Portrait](http://www.silhouetteamerica.com/shop/machines/item-number/silhouette+portrait),
and it's pretty freaking awesome.  In addition to using it for various
crafts, one thing we knew we would use it a lot for would be making
shadow puppets for the boys (for story time and for their own play).  
  
Almost immediately after she opened it, I started thinking of things *I*
could do with it.  Moments later I thought "this might be the most
ultimate paper airplane maker EVER!"  
  
![plane](/images/plane-parts.jpg)

As
soon as we came back home I grabbed the kit, and scanned my first plane.
 The software the Silhouette comes with is easy to use, and with
relatively little effort I imported the scan and detected the shapes of
the model components.  We had some appropriately thick paper lying
around, so I loaded the machine and set it to work.  It was fun to watch
it quickly and carefully cut out all the shapes on the page.  
  
Glueing the components together was quick and easy (don't forget to pile
some books on top to make sure your fuselage dries perfectly straight!)
 The finished product is beautiful, and if you ask me it's the best use
we've come up with yet for the "Portrait"!  (I'm still thinking of other
cool things we can use it for to cut and glue, and of course I'll post
here when I come up with more uses...)  
  
![plane](/images/paperplanes.jpg)
