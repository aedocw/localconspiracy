Title: How the f**k do magnets work?!?
Date: 2012-04-27 15:37
Author: Christopher
Tags: 
Slug: how-the-fk-do-magnets-work

I LOVE this excerpt from a Richard Feynman interview ([transcribed
here](http://lesswrong.com/r/discussion/lw/99c/transcript_richard_feynman_on_why_questions/#thingrow_t3_99c))
where he responds to a question about the "feeling" of magnets
repelling:  

> "Of course, it's an excellent question. But the problem, you see, when
> you ask why something happens, how does a person answer why something
> happens? For example, Aunt Minnie is in the hospital. Why? Because she
> went out, slipped on the ice, and broke her hip. That satisfies
> people. It satisfies, but it wouldn't satisfy someone who came from
> another planet and who nothing about why when you break your hip do
> you go to the hospital. How do you get to the hospital when the hip is
> broken? Well, because her husband, seeing that her hip was broken,
> called the hospital up and sent somebody to get her. All that is
> understood by people. And when you explain a why, you have to be in
> some framework that you allow something to be true. Otherwise, you're
> perpetually asking why."

It reminds me of nearly every conversation I have with my older son.
 Lacking a deep knowledge base with which to form a frame of reference
(though he does pretty well for a 3.5 year old), every answer leads to
yet another question.  Eventually, the chain always ends with something
like "well, that's just the way molecules work."  He hasn't started
asking "What's a molecule?" just yet, but I'm sure that one is right
around the corner...  
  
I hope he never stops trying to understand everything!

</p>

