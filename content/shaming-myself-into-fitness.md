Title: Shaming myself into fitness
Date: 2010-03-18 03:37
Author: Christopher
Tags:  biking, health, exercise, fitness
Slug: shaming-myself-into-fitness

A few years ago I would occasionally ride my bike to work (25 miles each
way). I stopped when we had Nate because there was no way I could get
home in time to have dinner with him if I rode. I also got pretty damn
lazy the first year after we (well, Heather) had him (and I sure put on
my fair share of baby weight!)  
  
Now that I've got a new job it's easier to shift my schedule around a
bit so heading for home on the early side won't be conspicuous and
frowned upon. It's also closer to home (15 miles each way), and the way
back home has a lot less uphill. That means the only real barrier is me.
Damn.  
  
THAT'S why I'm going to start talking about it here, and probably on
facebook and twitter (or maybe I'll just always talk about it here and
post links elsewhere...) The more people who know about it, the more
people there will be to give me crap if I don't follow through!  
  
Here's the deal: I'm going to start riding my bike to work in April. I'm
going to do it three days a week. With any luck, by the end of April
I'll be so used to it, riding my bike to work will have become a regular
habit.  
  
Wish me luck!  
  
-Christopher

