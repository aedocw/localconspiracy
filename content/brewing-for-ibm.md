Title: Brewing Beer for IBM
Date: 2017-09-05 15:56
Author: Christopher Aedo
Tags: beer, corporate shill, homebrew, open source
Slug: brewing-for-ibm

![TwitchTV logo](/images/new-twitchtv-logo.png)

On Monday, September 11th at 9am PDT I'm going to brew beer [live
on twitchTV](https://www.twitch.tv/events/89852)!  I'm as
surprised as anyone that I get to do this during working hours
and as part of my job.  Really looking forward to this :)

My employer (IBM) is trying to draw attention to some
unconventional things us IBMers do with open source software and
I think this definitely counts.  I've talked about [my electric
brewery](/2016/12/electric-brewery.html) here before.  This time
I'm going to demonstrate it live, talk a bit about the design,
and demonstrate in a generic way how you can use GPIO pins on a
Raspberry Pi to switch relays on an off.

Hopefully you can [join me](https://www.twitch.tv/events/89852)
on 9/11, 9am PDT!

