Title: Jeremy Grantham and the stats behind the end of the world
Date: 2013-05-26 21:35
Author: Christopher
Tags:  downfall of humanity, conspiracy, collusion, beating a dead horse
Slug: jeremy-grantham-and-the-stats-behind-the-end-of-the-world

OK, that's a sensationalist title and was not the topic of this
interview, but resource scarcity and the unbounded growth of world
population was discussed. It's absolutely worth putting this on and
paying attention. You will immediately be more informed on the stats
around the global economy than likely anyone else you know :)  
  
 Find [the video here](http://www.charlierose.com/view/interview/12812)
and [the transcript
here](http://www.scribd.com/doc/130919690/Charlie-Rose-Jeremy-Grantham-Transcript).  
  
Interesting quote:  

> When the price of resources are going down it makes getting -- getting
> wealthy much easier. And in total the typical commodity dropped by 70
> percent over a hundred years. And then it turned on a dime and gave
> the whole 100 years back between 2002 and 2008. In six years it gave
> back a hundred years of decline. It went up more steeply than it did
> in World War II. It's quite amazing no one talked about it, there was
> no fuss, there was no World War III. 

> But suddenly we seem to be running out of cheap resources. And when we
> look for the reason, incidentally, it seems to be steady population
> growth and perhaps more importantly, the enormous surge in China, 1.3
> billion trying to grow faster than the 20 million South Koreans did 20
> years ago, growing their demand for resources at 10 percent a year.
> And pretty soon you end up with numbers that don\`t seem to compute
> very easily.

> China uses 53 percent of all the cement used on the planet -- not
> traded, just used. They use 47 percent of all the coal, 46 percent of
> all the iron ore. These are unimaginable numbers. And if they mean to
> even slow down to seven percent, itmeans 10 years from now we\`ve got
> to find another 47 percent coal, just for China.

</p>

