Title: The $1k IPA
Date: 2015-08-03 11:15
Author: Christopher Aedo
Tags:  homebrew, beer, fail
Slug: the-1k-ipa

![1k IPA on tap](/images/1kipa-on-tap.jpg)

I have been brewing for a while now (around 10 years I think?),
and have always done 5 gallon batches, and kegged into corny kegs
rather than bottling. (Mainly because I love brewing, love beer,
and really like having beer on tap in the kitchen.) I have been
interested in stepping up to 10 gallon batches though, as the
time/effort are the same though you end up with double the beer.
Finally this year I thought I would up my game and start making
10 gallon batches.

To that end I figured the only thing I was going to need was a
bigger mash tun and a bigger carboy.  My brew kettle was a keg
with the top cut open and a valve at the bottom.  It served me
well for many years and seemed like it would be big enough.  I
bought a big cooler from costco and set it up with a screen and
valve, and thought I was good to go.

For my first 10 gallon batch I decided I would adjust one of my
favorite recipes, my single-hop Citra IPA. With a few tweaks, I
had what looked like it would be a pretty good beer - my [Citra
Single 10 gallon](https://www.brewtoad.com/recipes/citra-single-10g).

During the brew the mash tun was perfect. But somewhere along the
way I failed to notice something pretty important. My
keg-turned-kettle had a maximum volume of 50L. The pre-boil
volume for this batch was going to be 14 gallons, or likely even
a little over that. According to *math*, 50L == 13.2gal. Doing a
little more math, I discovered 13.2gal is less than 14gal, which
meant I was not going to have room for all the wort. It really
would have been nice if I'd done this maths before I started the
brew...

Unfortunately I did not think about the numbers until I was
draining the second runnings from the mash and noticed my kettle
was already pretty full. Ultimately I ended up dumping a gallon
of wort. I suppose I probably could have boiled the extra wort in
a second kettle with a few of the bittering hops and combined
that as I boiled the main batch, but at that point I thought the
only drawback would be lower alcohol volume for the finished
product, and all things considered that's nothing to be worried
about.

I had not picked up a large carboy for fermentation yet, so I
planned to use two 7gal glass carboys to do the primary
fermentation with the intention of buying a large food-grade
plastic barrel before it was time to transfer to secondary. One
of the glass carboys I had was a [big mouth
bubbler](http://www.northernbrewer.com/shop/brewing/brewing-equipment/fermenting-equipment/big-mouth-bubbler)
my wife had given me for my birthday. It's an excellent choice if
you are in the market for a big glass carboy. Once the beer was
boiling, I got to work sanitizing my fermenters.

I transferred about 3 gallons of starsan-solution to the big
mouth bubbler, and gave it a few minutes (while swirling it up
the sides a bit too). Intending to soak the top half in sanitizer
as well, I picked the carboy up to turn it upside down. Sadly,
this was when disaster struck. The thing slipped from my hand and
the bottom fell just a few inches onto the counter, which caused
it to basically explode. Shards of glass flew everywhere landing
as far as twelve feet away. All that sanitizer poured across the
counter, over the cooktop, onto the floor and down the steps into
the living room.

As the glass broke it cut my hand too (not badly, but enough to
need at least a little attention.) Our kids were drawing at the
kitchen table, and the dogs were curious about all the
commotion. So I grabbed a paper towel for my hand and then rushed
off to grab some towels for the sanitizer that was still running
town the steps, under the fridge, and across the kitchen floor.

Around the time I felt I finally had the situation under control
with towels all over the floor and the kids and dogs maintaining
a reasonable distance, I noticed how much water was on the
counter top. It was a lot of water still. There was also a bunch
of stuff ON the counter. Some paperwork (bills and stuff), and a
few library books. Oh look, my wife's macbook was also there -
oops!

In case you were not aware, macbooks are not water proof. Or
water resistant. In fact, they don't even like moisture, let
alone sitting in half an inch of water. As I picked it up off the
counter, a disturbing amount of water ran out of it. Really not
good.

*Relating this story to a friend, he responded:*
> "wow.  a day like that would end home brewing in my house."

Eventually I got everything cleaned up, and MOSTLY paid attention
to the beer that was still in-process. I cleaned and sanitized
two carboys and got the beer transferred and ready for the yeast.
Then I turned back to cleaning and drying stuff out. One library
book took a pretty bad hit, and was definitely water damaged. The
worst was the laptop though, and I had little hope of getting it
running again. I did open it up and dry it out but I was never
able to get it to fire up again. No data was lost (everything was
backed up with crashplan) but I definitely owed my wife a new
computer. AND the library made us replace the book I ruined
too.

Ultimately the beer turned out really good, so I'm looking
forward to brewing it again. With any luck the next batch will
not be quite so costly!

