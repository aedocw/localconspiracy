Title: tivo is for suckers
Date: 2010-09-21 00:02
Author: Christopher Aedo
Tags:  bittorrent, appletv, dvr
Slug: tivo-is-for-suckers

OK, that title is unfair. TiVo is pretty great, I bought a series 1 in
2000, and within the first year added a giant hard drive (120gb WAS
pretty big in 2001!) and a network card. A year later I got DirecTV and
the Philips TiVo that worked with DirecTV. That was when I started
experimenting with archiving and offloading content. None of the
experiments went really well.  
  
My goal was to keep my own archive of recorded shows, for as long as I
wanted, and be able to play them on any TV or computer in the house. I
also wanted to convert all our DVDs to mp4 and be able to scroll through
the movies and play them, like a pay-per-view system at a hotel. That
meant I would need a "back end" to store and deliver the content, and a
"front end" to browse and play the content.  
  
The first pass was marginally successful: I started ripping all my DVDs
to mp4, and storing them on a file server at home (the adventures in
file serving will be the subject of another nerdtastic post...) To
browse and play the content, I hacked an XBox and installed [Xbox Media
Center](http://en.wikipedia.org/wiki/XBMC). It worked reasonably well,
but since the XBox had limited memory and horsepower, it couldn't handle
decoding movies encoded with some of the better codecs (like
[h.264](http://en.wikipedia.org/wiki/H.264) which was at the time
gaining popularity, and improving rapidly.)  
  
The TiVo was a pretty poor primary source for capturing shows since it
encoded everything in MPEG2, so for long term storage and reasonable
tranfer rates you had to transcode everything, which just took too long.
It was also a bad choice for the front-end because you couldn't use it
to play media from an external source (like the file server that had all
my movies) unless it was mpeg2 (AND you had to transfer it to the tivo,
it wouldn't stream directly from the source.)  
  
I played around with [MythTV](http://www.mythtv.org/) for a while, but I
think the "myth" part of the name refers to the myth that it actually
works. (I know I know, it DOES work, just not reliably or easily.) In
theory it would have been a pretty great system, as they used the
backend/frontend model from the beginning, and they offered players on
multiple platforms, so it should have been relatively easy to build up a
PC for however many TV's I wanted to play the content on. As I started
working on it though, things just got too complicated. Ideally I would
have three DirecTV tuners connected to it, and be able to capture three
shows or movies at the same time. I got it working well enough with one,
but the whole thing seemed to flaky and fragile, and I needed something
that would always just work, and that my wife and friends could use with
little or no training. MythTV was not the answer.  
  
Ironically, the approach that answered all my questions was prompted by
me taking the first steps towards getting rid of TV altogether. I
convinced my wife to go along with me on canceling our DirecTV
subscription by promising we would still be able to get the handful of
shows that we wanted to watch, and she would not have to sit in front of
a computer to watch them.  
  
While learning about MythTV and the community of people working
diligently (and without pay) to improve it, I discovered that a great
many of them were sharing the shows they captured via
[bittorrent](http://en.wikipedia.org/wiki/BitTorrent). Though an
argument can be made that it's stealing, if you're just talking about
watching over-the-air shows it's no different than watching it after
capture on your tivo, and using the 30-second-skip trick to jump past
commecials. (If you're talking about pay-tv stuff, like HBO shows, etc.,
well, then you are stealing... But maybe it's OK if you ultimately buy
the DVD's when they're available?)  
  
So the shows were out there, but was there an easy way to grab them as
they became available? Absolutely! The bittorrent client Vuze is the
best one out there, and there are [hundreds of
plugins](http://azureus.sourceforge.net/plugin_list.php) written to
extend it's usability. One plugin, [RSS Feed
Scanner](http://azureus.sourceforge.net/plugin_details.php?plugin=rssfeed),
specifically addressed my needs, and worked beautifully. Then you just
have to find your RSS feeds, though you can probably find everything you
need from [EZTV](http://eztv.it/). 
[EDIT: I withdraw my support of Vuze after a
forced upgrade wiped out all my RSS settings! I replaced it with
[utorrent](http://www.utorrent.com/downloads) and found it's far better,
faster, and has RSS support built right in!]
  
With the content side sorted out, what was the best way to watch it? The
XBox worked for some stuff, but not always. It was also occasionally
crashy, and would sometimes lock up right in the middle of a show. Thats
no bueno...  
  
The answer turned out to be
[AppleTV](http://store.apple.com/us/browse/home/shop_ipod/family/apple_tv?mco=MTE4MTU)!
It required a slight modification since Apple would rather you watch
things you get from the iTunes store, but there's really nothing to it.
There are a ton of things you can do to the AppleTV and there's [lots of
information](http://www.appletvhacks.net/) out there on the subject. All
you really need to do is enable SSH (for easy remote access), then
install the divx and xvid codecs and a file browser. [Engadget had a
good article about
it](http://www.engadget.com/2007/04/10/how-to-play-divx-and-xvid-on-your-apple-tv/),
and I found other sites/guides as well (email me if you have any
questions, I'll be happy to help!)  
  
We've been using this setup for almost two years now, and it's worked
almost without incident. You can get AppleTV's on ebay pretty cheap, so
there's no big barrier to adding players to multiple TV's throughout
your house. As long as you don't mind waiting a few hours until after it
airs to catch a show, your only ongoing cost is the bandwidth. There
might be problems down the road (ISP's don't like bittorrent because
they promised you more bandwidth than they have, and bittorrent has a
habit of using up the bandwidth available...)  
  
-Christopher

