Title: No news is good news
Date: 2012-09-26 03:26
Author: Christopher
Tags:  random, necessary skills, beating a dead horse, improvement, be better
Slug: no-news-is-good-news

> "Chances are you have one of the most insidiously dangerous objects
> ever created ' and one of the State's most powerful tools of
> domination ' in your living room right now. You may very well have one
> in your bedroom, too, and in your basement.  
> It's insidious because most Americans don't think of them as
> dangerous, nor do they think of them as tools of State domination;
> otherwise they wouldn't have them in their homes. In fact, most
> Americans love them. Most can't go a single day without them. You
> might even say they were addicted to them." ([Craig Russel, "Turn It
> Off"](http://www.strike-the-root.com/3/russell/russell4.html))

  

> "The trouble with the news is that everybody knows everything too fast
> and too often and too many times. News had always been bad. The tiger
> that lives in the forest just ate your wife and kids, Joe. There are
> no fat grub worms under the rotten logs this year, Al. Those sickies
> in the village on the other side of the mountain are training hairy
> mammoths to stomp us flat, Pete. They nailed up two thieves and one
> crackpot, Mary. So devote wire service people and network people and
> syndication people to gathering up all the bad news they can possibly
> dredge and comb and scrape out of a news-tired world and have them
> spray it back at everybody in constant streams of electrons, and two
> things happen. First, we all stop listening, so they have to make it
> even more horrendous to capture our attention. Secondly we all become
> even more convinced that everything has gone rotten, and there is no
> hope at all, no hope at all. In a world of no hope the motto is
> *semper fidleis*, which means in translation, "Every week is
> screw-your-buddy week and his wife too, if he's out of town." (John D.
> MacDonald/A Tan and Sandy Silence)

  
  

</p>

