Title: Pancakes
Date: 2011-08-21 17:06
Author: Christopher Aedo
Tags:  pancakes, cooking, food
Slug: pancakes

UPDATE: There's an updated version of the recipe that is even tasier
and slightly healthier too, check out [the updated
recipe here](/2018/01/pbpancakes.html)!

When I was growing up, almost every weekend my mom would cook these
awesome pancakes for breakfast. They were a little different (and way
better) than the standard variety, being made with more eggs than usual,
and cottage cheese. She never looked at a written recipe when making
them, so I always assumed these were from some old family recipe either
from Germany or Lithuania.  
  
When I moved out of the house and was living on my own for the first
time, I was looking forward to making these pancakes myself. She read
the recipe to me (from memory) over the phone, and I wrote it on some
scrap of paper. A year or three later I lost the scrap, and hadn't made
them often enough to have memorized the recipe, so I asked for the
recipe again.  
  
This time, in the course of the conversation I asked where the recipe
had come from, wondering if it had been her own mother. Hilariously, she
told me "Oh I got the recipe from the top of a tub of cottage cheese a
long time ago."  
  
Secret family recipe my ass!  
  
They ARE fantastic pancakes though; I made them again for breakfast this
morning, and thought I should share the recipe with my friends, so here
you go! (This version is slightly modified from the one my Mom used to
make, I substitute oats for flour and add bananas for improved awesomeness).

> Aedo Family Secret Pancake Recipe:  
> 1 cup cottage cheese  
> 6 eggs  
> 1 and a 1/2 cup dry quick-cook oatmeal  
> 1/2 teaspoon salt  
> 1/4 cup olive oil  
> 1/2 cup rice milk  
> 1/2 tsp. vanilla extract  
> 2 bananas  

Place ingredients in blender, blend until smooth, then cook like reglar
pancakes.

Enjoy!

</p>

