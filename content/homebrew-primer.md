Title: Homebrew Primer
Date: 2009-08-19 21:51
Author: Christopher Aedo
Tags:  homebrew, beer, necessary skills
Slug: homebrew-primer

I wrote this up for my friend Moshe, and have since forwarded it to
probably five or six people who are interested in brewing their own
beer. I thought I might as well put it up here, should make it a lot
easier for me to share!  
  
WOW, here's my home brew primer. It's long, but if you read it all you
will have all the info you need to get started. You could get most of
this same information from the book [The New Complete Joy of Home
Brewing](http://www.amazon.com/gp/product/0380763664?ie=UTF8&tag=httplocalcons-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=0380763664),
which is a pretty good book to start with. You could also just go to
<http://www.homebrewtalk.com/> and check out their forum where you can
find a few good starter guides.... One other good book I could suggest:
[How to Brew: Everything You Need To Know To Brew Beer Right The First
Time](http://www.amazon.com/gp/product/0937381888?ie=UTF8&tag=httplocalcons-20&linkCode=as2&camp=1789&creative=390957&creativeASIN=0937381888).
  
BREWING STEP ONE:  
The three main methods of home-brewing are "extract", "partial-mash" and
"all grain".  
  
Extract brewing uses malt extract (either liquid or dried malt extract
[aka DME]). This malt extract is a syrup-like (or powdered)
concentration of the sugars contained in the grain. It's extracted from
the grain and concentrated under pressure, and sold by the pound.
Brewing with extract is easy, and requires a smaller brew pot (because
you only need to boil 3 gallons or so of liquid since the malt is highly
concentrated - you add water to bring your total volume up to 5 gallons
AFTER you've done all the brewing...) The only drawback is that it's not
considered "pure" brewing, since someone else has done some of the work
for you. SOME people claim they can taste the difference between an
extract and all-grain brew, but most people would never know the
difference. Extract brewing is AOK... You need a brew pot at least 4 or
5 gallons for this.  
  
Partial-mash uses extract plus a small amount of specialty grains used
to influence the beers flavor to match the particular style. For
instance, a recipe might call for six pounds of pale malt extract, plus
another pound of some specific grains. These grains will be steeped in
160 degree water for 20-30 minutes, and then that liquid will be poured
through a strainer into your beer. For best results, those leftover
grains would then have 170-200 degree water poured over them to rinse
the last of the sugars off the grain (this process is called sparging).
Some recipe kits come with a mesh sock to put the grains in, so you
would not need to strain the grains out, just pull out the sock. This
method makes it a little harder to "sparge" the grains, but you're
really not getting much sugar out of such a small amount of grain
anyway, so it probably doesn't matter. You need a brew pot at least 4 or
5 gallons for this.  
  
Finally, there is "all grain" brewing, where you start with a big bag of
ground grain (10lbs up to 20lbs depending on the beer, though the more
grain the higher the gravity/alcohol content), extract all the sugars
yourself, and then proceed from there the same as you would have with
any other method. Getting the sugars out is called "mashing", and can
seem complicated and daunting at first. In short though, you need to
control the temperature of the grain at two different temperatures, and
possibly three or more depending on the type of grain and the specific
recipe you are using. There are entire books about this process, so I
don't think I can give it justice here in a paragraph or two, so I won't
even really try. The main consideration for someone just getting started
is that all-grain brewing requires more (and larger) equipment. First
off, you need a boil kettle that can handle at least 7 gallons, or
possibly more. Many people convert a half-barrel keg (like the kind you
would rent for a party) into a brew kettle, since they're about 15
gallons in size. That also means you probably have to boil your beer
outside unless you have a really large cooktop. People use the burner
setup that comes with a turkey fryer (available from home depot most
commonly.) So you need at least one large kettle, possibly two. One will
be your Hot Liquor Tank (HLT), where you heat up the water for the
grains. The other would be your brew kettle where you boil your wort
(the sweet liquor you extracted from the grains, pronounced "wurt"). I
just have one large kettle, and find that to be pretty easy. Then you
need a large (7-10 gallon) water cooler to hold the grain and water
during the mash. These coolers work pretty well since they hold the
temperature for a long time. If you don't want to use a cooler, and have
a large pot available, you can keep the grains in the large pot and heat
them directly. This can be tricky though, as you risk scorching the
grains while applying heat - this can give your beer some off flavors.  
  
Wow, that's already a lot about all-grain, I think I'll let it rest
there. I can talk for a while about this in person, and show you the
setup I'm using ([where you use steam to heat the
grain](/2009/08/mashing-with-steam.html)),
it works really well and was not terribly expensive.  
<span style="font-weight: bold;">  
BREWING STEP TWO</span>:  
Now you've got your wort and it's either 3 gallons or 6 (or more if
you're doing a large batch or brewing a high gravity beer that has a lot
of volume you need to concentrate.) Let's assume you're doing an extract
brew as that's the easiest. You want a vigorous rolling boil during this
hour, and the timer starts once you hit the boiling point. Most recipes
call for a one hour boil during which time you will add hops to the wort
according to a schedule. You'll usually add some hops at the beginning,
then after 45 minutes add more, and finally there will be your bittering
hops added right at the end after you've turned off the heat.  
<span style="font-weight: bold;">  
BREWING STEP THREE</span>:  
You've boiled your wort for an hour, now you need to cool it down to
about 70 degrees, add water to bring it up to the necessary volume, add
your yeast, and then wait! There are a few different methods you can use
to cool the wort. Simplest (and I've done this many times) is to
transfer from your boil pot into a fermentation bucket that already has
a bunch of ice in it. The boiling wort will melt the ice and if you're
lucky, it will stabilize around 70 degrees. I usually would just cover
it up and let it sit until it got to the right temperature at this
point. I also made a "chiller", which is a coil of copper tubing through
which you pump cold water. You put the tubing in the wort, and keep
recirculating cold water through it while occasionally stirring the
wort. You can also buy a counter-flow chiller, a chillerator or a
chillzilla.... There are many options, and most of them get pretty
expensive.  
  
OH, before pitching the yeast, it's really important to oxygenate the
beer. You can do this by shaking the hell out of the bucket or carboy,
or by pumping oxygen through a small stone with either an oxygen tank or
an aquarium pump.  
  
Anyway, once it's chilled, you put it in a bucket or glass bottle (aka
carboy) for fermentation. Pitch your yeast into the mixture, and seal it
with an airlock so the gas the yeast creates can escape without allowing
air from the outside to get back in (which can contaminate your brew.)
After a week or so of fermenting, I usually transfer to another carboy
using a siphon. This is so you can leave the now dead yeast at the
bottom of the first vessel, and let it sit for another week or two in a
new vessel, allowing all the suspended grains and yeast to settle to the
bottom. This helps you get a clear beer, so there won't be any sediment
in the bottom of the glass when you're drinking it a few weeks later.
During fermentation, every strain of yeast will have a specific
temperature range that it performs best in. The recipe will usually
suggest a yeast or two, and will also usually say what the optimum
fermentation temperature is. I've been using a small wine fridge for
fermentation, it's convenient and keeps the beer at whatever temp you
set on the front.  
  
After fermentation is all done, you can either bottle the beer, or put
it into a keg. If you are going to bottle, you need to add some sugar to
the beer again. Once the beer is sealed in a bottle, the small
additional sugar will wake up the yeast again and carbonate your beer.
If you put too much sugar, you might end up with bottles that explode
due to excessive pressure.  
  
If you're going to keg (by far the easiest way) you just transfer the
beer into the keg, and seal it up. Then you cool it to 40 degrees, and
pressurize with your CO2 canister. After three days to a week, the beer
will have absorbed the CO2 and will be carbonated and ready to drink!
Some beers are ready at this point (roughly three weeks from when you
brewed), other beers can take 3 to 6 months to properly condition (they
won't taste quite right if you don't wait long enough with some of the
more complicated beers.) ECONOMY: You can expect to spend from \$70 to
\$500 on your initial investment. Spending a lot up front probably means
you won't need to buy anything but ingredients for a while.  
  
Good sources for starter kits:

-   [Coopers complete starter
    kit](http://makebeer.net/item.asp?idProduct=1&idCategory=1&idSubCategory=0)
-   <http://www.northernbrewer.com/starterkits.html>

  
  
At the bottom of that Northern Brewer page, their ultimate kit would get
you started with everything you need, including keg. You might be able
to piece things together for less, or work your way up to having all the
pieces over time. I guess it depends on how much you want to invest in
the beginning.  
  
One gallon is 128 fluid ounces, which means a five gallon keg contains
53 12oz beers. Assume \$10 for 12 "premium" beers, that's about \$44
worth of premium beer in a keg. The ingredients would cost around \$30,
so the more beer you drink, the more money you save! You can spend more
(\$40-\$45) on ingredients, but that's for a pretty serious brew, and
then you're talking about copying a beer like Arrogant Bastard Ale which
I think costs \$10 for just a six pack! That means your keg would hold
roughly \$90 worth of beer!!  
  
A good starting point would be the extract recipe kits they sell at
Northern Brewer, take a look at them on their site. Another good
alternative is to come in to the brew shop in Culver City and use one of
their recipes. They have all the ingredients there, and help put the kit
together if you're using one of their recipe packages.
