Title: Social networking is anti-social
Date: 2011-12-11 19:00
Author: Christopher
Tags: 
Slug: social-networking-is-anti-social

For a brief time, I was bullish on social networking. I blame it on my
natural human need to maintain consistency, since I was working at a
company that was tangentially involved with social networking (i.e. if
you don't make yourself believe at least a little bit in what you're
doing, you're going to be miserable.) Before that time, and after, I've
been pretty thoroughly anti all things "social networking". My views on
the matter are definitely not just a result of being an old curmudgeon -
I just read two good articles on this very subject, thought you might
enjoy them as well...  
  
What's wrong with the idea of "frictionless" sharing, where every action
is automatically broadcast to your network of friends?  

> Facebook's "frictionless" sharing doesn't enhance sharing; it makes
> sharing meaningless. Let's go back to music: It is meaningful if I
> tell you that I really like the avant-garde music by Olivier Messiaen.
> It's also meaningful to confess that I sometimes relax by listening to
> Pink Floyd. But if this kind of communication is replaced by a
> constant pipeline of what's queued up in Spotify, it all becomes
> meaningless. There's no "sharing" at all. Frictionless sharing isn't
> better sharing; it's the absence of sharing. There's something about
> the friction, the need to work, the one-on-one contact, that makes the
> sharing real, not just some cyber phenomenon.
> (<http://radar.oreilly.com/2011/12/the-end-of-social.html>)

An excellent perspective on the meaning of friendship throughout
history, and the tragic direction it's heading these days:  

> As for getting back in touch with old friends—yes, when they're people
> you really love, it's a miracle. But most of the time, they're not.
> They're someone you knew for a summer in camp, or a midlevel friend
> from high school. They don't matter to you as individuals anymore,
> certainly not the individuals they are now, they matter because they
> made up the texture of your experience at a certain moment in your
> life, in conjunction with all the other people you knew. Tear them out
> of that texture—read about their brats, look at pictures of their
> vacation—and they mean nothing. Tear out enough of them and you ruin
> the texture itself, replace a matrix of feeling and memory, the deep
> subsoil of experience, with a spurious sense of familiarity. Your
> 18-year-old self knows them. Your 40-year-old self should not know
> them. (<http://chronicle.com/article/Faux-Friendship/49308/>)

  
  

</p>

