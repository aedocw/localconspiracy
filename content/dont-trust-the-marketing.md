Title: Don't trust the marketing!
Date: 2012-04-29 01:07
Author: Christopher Aedo
Tags:  exercise, corruption, fitness, cooking, food
Slug: dont-trust-the-marketing

The harder someone tries to sell you something, the more suspicious you
should be.  Why don't more people realize that?  When something is truly
great it sells itself.  When it doesn't, throw marketing at it!  Got
milk?  Seen a McDonalds add lately?  
  
If you've ever wondered why food makers switched from using animal fats
in cooking to using factory-produced abominations, now you know.
 Profit, pure and simple.  

> "Convincing homemakers to swap butter and lard for a new fat created
> in a factory would be quite a task, so the new form of food needed a
> new marketing strategy. Never before had Procter & Gamble -- or any
> company for that matter -- put so much marketing support or
> advertising dollars behind a product. They hired the J. Walter
> Thompson Agency, America's first fullservice advertising agency
> staffed by real artists and professional writers. Samples of Crisco
> were mailed to grocers, restaurants, nutritionists, and home
> economists. Eight alternative marketing strategies were tested in
> different cities and their impacts calculated and compared.  
> Health claims on food packaging were then unregulated, and the
> copywriters claimed that cottonseed oil was healthier than animal fats
> for digestion. Advertisements in the Ladies' Home Journal encouraged
> homemakers to try the new fat and "realize why its discovery will
> affect every family in America." ([from The
> Atlantic](http://www.theatlantic.com/health/archive/2012/04/how-vegetable-oils-replaced-animal-fats-in-the-american-diet/256155/))

This new fat discovery certainly did affect every family in America!
 Here we are 100 years later and we're [the fattest nation in the
world](http://www.huffingtonpost.com/2012/02/22/obesity-rates-rising-developed-fattest-world_n_1294212.html#s716476&title=1_United_States).
 We're number one!  GO TEAM!

</p>

