Title: Whats On Tap, February 2018
Date: 2018-02-08 20:16
Author: Christopher Aedo
Tags: beer, kitchen taps
Slug: on-tap-feb2018

![image](/images/on-tap-feb2018.jpg "four beer taps")

I am going to start putting up a little post whenever what's on
tap at home changes. I will also make an effort to post when I
brew something as well.  To that end, here's the first post on
that subject!

The Porter I brewed came out really nice. When I first kegged it
I was kind of disappointed, but I really should have just
remembered it was REALLY green, and give it a little time.
Within a few weeks it started to mellow and taste really good.
The only thing I would do different is a little more yeast (no
starter and I pulled it off thee yeast early so FG was 1.020) and
it could use a little vanilla.

The Belgian Dubbel is good! It tastes a little better every week,
but it's really tasty as is.  Next time I'm going to use some oak
chips, and I'm going to try natural carbonation in the keg
instead of force-carbing.

Schwartbier is easy drinking and smooth. Not much more to say
other than it's pretty refreshing and has some nice malt notes.

The Pale Ale turned out great - this one probably won't last that
long.  Next time I might put in a little flaked rye for a touch
more mouthfeel?  Maybe not though, as it stands up well as-is.

Fermenting right now is my copy of Arrogant Bastard which I call
"Conceited Jerk".  It will be ready to be kegged this weekend,
and will wait patiently in the keggerator for a free tap.  Recipe
below!

```
Recipe: Conceited Jerk (5g)

Ingredients:
------------
Amt                   Name                                     Type          #        %/IBU         
12 lbs                Pale Malt (2 Row) US (2.0 SRM)           Grain         1        76.2 %        
1 lbs 4.0 oz          Roasted Barley (300.0 SRM)               Grain         2        7.9 %         
1 lbs                 Caramunich Malt (56.0 SRM)               Grain         3        6.3 %         
1 lbs                 Oats, Flaked (1.0 SRM)                   Grain         4        6.3 %         
8.0 oz                Special B Malt (180.0 SRM)               Grain         5        3.2 %         
1.50 oz               Chinook [13.00 %] - First Wort 60.0 min  Hop           6        60.7 IBUs     
0.75 oz               Chinook [13.00 %] - Boil 15.0 min        Hop           7        13.7 IBUs     
0.75 oz               Chinook [13.00 %] - Steep/Whirlpool  15. Hop           8        6.8 IBUs      
2.0 pkg               Safale American  (DCL/Fermentis #US-05)  Yeast         9        -             
1.00 oz               Chinook [13.00 %] - Dry Hop 5.0 Days     Hop           10       0.0 IBUs      

Mash Schedule: HERMS 151
```
