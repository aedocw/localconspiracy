Title: Christopher Aedo's Resume (doc@aedo.net)
Date: 2012-01-01 10:00
Author: Christopher Aedo
Tags: resume
Slug: christopher-aedo-resume


# Summary
Empathetic and people-first Technology Executive, dedicated to fostering a safe, inclusive environment that drives curiosity, candor, and continuous learning. Recognized for my ability to structure and align teams, accelerating delivery timelines and establishing transparent product roadmaps. Product-led engineering approach with a strong capacity for strategic decision making, tactical planning, and closing customer deals. Proud to lead with kindness, leveraging comprehensive insight from across the organization to improve inter-departmental cooperation and strategic direction.

# Skills

Strategic Planning
// Team Collaboration
// Mentorship and Hiring
// Performance Evaluation and Optimization
// Agile Development
// Software Engineering Leadership
// Config and Infrastructure as Code
// Automation
// Cloud Computing
// Kubernetes, Docker
// Infrastructure Deployment, CI/CD
// GraphQL
// Terraform, Ansible
// Process Optimization
// DevOps
// AWS, Azure, GCP
// Keynote Speaker

# Experience
### VP of Software Engineering
Pandemic Response Labs/Opentrons Labworks Inc. - NY

04/2022 - 03/2023

- Restructured 20 person SW engineering team & established career ladder, increased team capacity by 20% by instituting pragmatic prioritization and drastically reducing unplanned work.
- Accelerated release cycle from 6w to 2w by reducing scope and improving planning and prioritizing, resulting in fewer bugs and eliminating the need for quick-fix releases.
- Mentored and worked hand in hand with the product management team to build and publish  the organizational roadmap, ensuring engineering priorities and timelines served business needs and reducing time to feature delivery by 15%.
- Outlined and sustained a technology strategy to support highly automated lab processes to maximize operational efficiency with the lowest possible turnaround times.
- Cultivated an empowering engineering culture providing inclusiveness and safety, enabling team members to express and harness their full creative abilities.


### Director, Project Management // Open Source

Teradata - Rancho Bernardo, CA

11/2017 - 04/2022

- Headed product management for Teradata's Extract/Transform/Load suite and overall data management tools.
- Wore multiple hats as an infrastructure/provisioning product manager and technical product owner of the foundational provisioning and deployment tools, a key aspect in Teradata's critical pivot to a subscription-based SaaS model.
- Supervised transparent roadmaps of multiple integrated projects, juggling engineering priorities to best meet the needs of both internal and external customers, with the organization's needs at the forefront.
- Instituted an Open Source Program Office within Teradata, a designated locale where open source was nurtured, shared, explained, and grown.
- Bridged the gap between legal, marketing, and engineering teams to share useful internal projects with the wider world by open sourcing them.
- Joined forces with internal engineering teams benefiting from open source software to enhance their integration with upstream communities and foster greater involvement.

### Developer Advocacy Program Manager

IBM - Armonk, NY

11/2016 - 11/2017

- Oversaw an 18 person team of open source developers dedicated to transforming IBM's global developer community perception by becoming vanguards for cutting-edge technologies.
- Interfaced with marketing and events teams to optimize sponsorship and marketing around IBM’s developer-focused initiatives.
- Boosted IBM’s presence and relevance in developer communities by securing sponsorship and organizing multiple conferences where IBM previously had minimal presence.


### OpenStack Innovation Team

IBM - Armonk, NY

08/2015 - 11/2016

- Deployed a distributed, scalable cloud based on OpenStack, continuously updated from upstream master to underpin IBM’s cloud compute offering.
- Leveraged Gerrit for code review, and automated sync of local changes with upstream commits to increase contribution and minimize stability issues.
- Accelerated the development velocity of 200 globally distributed engineers by implementing and training them on a revolutionary platform utilizing modern CI/CD, code review and automation.


### Product Architect

Mirantis - Mountain View, CA

03/2014 - 07/2015

- Partnered with product management team, engineering teams, Mirantis' customers, and OpenStack contributors to provide strategic leadership, aligning short and long-term product goals with market needs and code/community directions.
- Conducted analysis of competitive and complementary products and technologies, critical for product, sales, and marketing teams' decision-making.
- Collaborated closely with documentation and training teams to create a robust user and contributor base.
- Fostered the local developer community by not only contributing to OpenStack but also organizing the Portland OpenStack User Group.


### Chief Technology Officer

Morphlabs - Manhattan Beach, CA

08/2011 - 01/2014

- Built and directed remote distributed engineering and support teams (8 US, 50+ international)
- Provided insightful technical stewardship to sales and delivery teams.
- Negotiated and fostered strategic partnerships and solution certification agreements with Ubuntu, Dell, NEC, Hitachi, and Media Temple.
- Championed the product owner’s perspective and steered the engineering team towards anticipatory industry needs.
- Instilled agile development and navigated the team towards fully automated Puppet-based deployments and updates, and continuous integration with Jenkins.
- Conceived a 24/7 customer support product and orchestrated a globally distributed support team.
- Shared industry insights by presenting at conferences and meetups from Los Angeles to Tokyo, Okinawa, and several OpenStack design summits.

# EDUCATION

- University of Illinois at Chicago, Mathematics/Computer Science


# VOLUNTEER & OPEN SOURCE

- Court-Appointed Special Advocate (CASA), appointed by the court to advocate for the best interests of children in foster care.
- Created [https://github.com/aedocw/epub2tts](https://github.com/aedocw/epub2tts), which creates an audio book from an epub or text file using generative AI for text-to-speech.
