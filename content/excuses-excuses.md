Title: Excuses, excuses...
Date: 2010-04-19 21:23
Author: Christopher Aedo
Tags: 
Slug: excuses-excuses

Last week (4/12 - 4/16) I only managed to ride one day:  

-   [To work](http://connect.garmin.com/activity/29895034)
-   [Back home](http://connect.garmin.com/activity/29895036)

It was not fun times. It had rained the night before, so the morning was
cold and wet (though I know it's nothing compared to the weather you
peeps in Chicago get to ride in!) On the way home it was raining for the
beginning of the ride, and then I had a headwind for the rest of the
ride. I think it was my slowest riding yet.  
  
The next day I woke up with a cold (Heather and Nate had a cold the week
before and I though I'd escaped, but I guess not!) Wednesday I felt even
worse and ended up taking a sick day. By then I realized there was no
chance of putting in three rides this week, so I scrapped it. Looks like
I'll be going into May just to make sure I get in four weeks of 3-a-week
rides to work :)  
  
At least I rode today - I'm pretty sure this week will be 3 for 3.

</p>

