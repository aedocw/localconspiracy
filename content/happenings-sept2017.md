Title: Speaking, streaming and brewing
Date: 2017-09-08 16:05
Author: Christopher Aedo
Tags: beer, homebrew, speaking
Slug: happenings-sept2017

![Science Hack Day PDX](/images/shd2017banner.png)

Tomorrow and Sunday PSU is hosting [Science Hack Day
PDX](http://portland.sciencehackday.org/). This free to attend
avent is going to be a blast, I'm really looking forward to it.
I will be there checking out what people are working on, and
[interviewing them on Twitch](https://www.twitch.tv/ibmcode). We
will likely end up editing together the highlights for later use
on
[https://developer.ibm.com/code/](https://developer.ibm.com/code/).
I expect there will be a bunch of interesting projects in
progress so should be super fun.

**UPDATE:** All the interviews went really well and were super fun,
the whole list [can be found on YouTube
here](https://www.youtube.com/watch?v=Q5bxA3wttQs&index=1&list=PLpryjkO3KF2zD1tnTNvr9deOe45UGHGG5).
I have to admit I had such a good time doing this I am going to
try to make a habit of it!

On Monday 9/11 at 9am PDT [I will brew beer and talk brewing on
Twitch](https://www.twitch.tv/events/89852).  I'm still a little
bit surprised about this one but hopefully it goes well and I
have an excuse to do it again one day.  Getting paid to brew has
always been kind of a dream - maybe I'm not going pro but for one
day, it feels pretty close!  I decided to brew a Cascadian Dark
Ale for the event; I put [the recipe](/images/cda.pdf) up if you
want to see what I'm making.

**UPDATE:** the session went great, and [can be found on YouTube
here](https://youtu.be/Xabt-klFv90?t=5m45s)

![SeaGL Logo](/images/seagl-logo.svg)

Finally, I'm super excited to be [speaking at SeaGL
2017](https://osem.seagl.org/conferences/seagl2017/program/proposals/278)
in a little more than a month! I also [answered a little
Q&A](http://seagl.org/news/2017/09/08/QA-caedo.html) about the
talk.  It should be a lot of fun to talk about my brewing system,
and how using a Raspberry Pi has changed how I make beer (and in
my opinion gone a long way to improve it).

Hopefully I'll see you at PSU, SeaGL, or online for one of the
Twitch streams!
