Title: All the pieces matter
Date: 2012-04-29 15:24
Author: Christopher Aedo
Tags:  downfall of humanity, improvement, the wire, be better
Slug: all-the-pieces-matter

This David Foster Wallace speech ([read
here](http://moreintelligentlife.com/story/david-foster-wallace-in-his-own-words),
or [watch here](http://www.youtube.com/watch?v=M5THXa_H_N8)) is one of
the most inspirational things I've ever read.  It's great to be reminded
to be mindful, to be aware of those knee-jerk reactions and consider
whether or not they're completely off base.

> "The
> point is that petty, frustrating crap like this is exactly where the
> work of choosing is gonna come in. Because the traffic jams and
> crowded aisles and long checkout lines give me time to think, and if I
> don't make a conscious decision about how to think and what to pay
> attention to, I'm gonna be pissed and miserable every time I have to
> shop. Because my natural default setting is the certainty that
> situations like this are really all about me."

On the same topic (making a
conscious choice about what and how you'll think), [this amazing essay
by David Simon](http://davidsimon.com/i-meant-this/) talks about missing
the point - like if watching The Wire inspires you to bet on who the
coolest character is (rather than discuss social injustice and the decay
of humanity).  Or talking about a vigilante in Florida without
considering the astounding legal precedent being set in the Trayvon v.
Zimmerman case...  It's an amazing read, and should get you thinking
(and talking).

> "A
> week or month or a year from now, someone else is going to walk up to
> a fresh victim in Florida or some other state burdened with
> stand-your-ground absurdity and we’ll have a new body over which to
> argue. Which is just fine, because does anyone really believe that our
> instant-assessment, instant-gratification media world  is capable of
> anything beyond the ad hominem?  Let them begin again and do what they
> do best:  Which one was the asshole?  Who is the bigger dickhead?  He
> deserved it.  No, he didn’t.  Which one am I rooting for?  Which one
> gets my vote?  Who wins?  Who loses?"
