Title: More people finally catching up!
Date: 2010-05-02 04:59
Author: Christopher Aedo
Tags: 
Slug: more-people-finally-catching-up

I just wanted to gloat a little bit. In 2007 we told DirecTV to suck it.
We cut back on our TV watching anyway (because watching less is good for
you, seriously!) What we did want to watch, we were able to [get from
the
Internet](/2009/05/tivo-is-for-suckers.html).  
  
LA Times pointed out more people were doing that [in this article from
October
2009](http://www.latimes.com/business/la-fi-notv26-2009oct26,0,3559474.story).  
  
Just yesterday, [CNN Money said one in eight people will drop their
cable this
year](http://money.cnn.com/2010/04/30/technology/dropping_cable_tv/) in
favor of using the 'net...  
  
Hopefully we'll break our ridiculous TV habit this year. Good reading on
why we should do that here...
<http://www.strike-the-root.com/3/russell/russell4.html>

