Title: Setup update
Date: 2014-07-21 22:14
Author: Christopher Aedo
Tags:  thingsilike, necessary skills, appletv, be better
Slug: setup-update

I'm posting this mostly for my own benefit, but I also realize it's been
SO LONG since I last posted, it will prove this blog is not entirely
abandoned!\*  
  
I outlined [my
setup](/2014/02/the-setup.html) at home in
February, and since then the only significant change has been firewall
tuning.  For months, I had intermittently difficulties with our internet
connection.  Chalking it up to a bad ISP, I didn't really bother to try
to make it better.  Occasionally I'd adjust the traffic shaping rules in
hopes of improving the situation, but it was never clear whether or not
the changes had a lasting positive impact.  
  
Then the other day I was reading about community mesh networks and was
reminded of "[buffer
bloat](https://en.wikipedia.org/wiki/Bufferbloat))". Checking my
connection with the [ICSI Netalyzer](http://netalyzr.icsi.berkeley.edu/)
showed there was some significant bloat going on, AND the ISP was
fragmenting traffic without properly reporting it. Switching my PFSense
queues to Codel and [tuning my MTU](http://www.dslreports.com/faq/695)
has resulted immediately in tremendously improved connectivity.  
  
All along I think the biggest issue was the fragmentation two hops away.
 Should have tested for that the first time I got connected!  Hopefully
writing this little post up will remind me next time.  
  
ALSO, under the topic of "the setup" and "things I hope I won't forget
again"...  When I set up my workstation at home I added a mac mini for
development work.  Recently I installed Ubuntu 14, and am absolutely
loving it.  Coupling that with [Synergy
Project](http://www.synergy-project.org/) for keyboard/mouse control
leaves me with a perfect setup.  I've got a decent monitor connected to
the min with my laptop next to it, and any time I want to control that
computer I just swing the mouse over.  It's like extending you desktop
onto a full-screen VNC session, except it's much smoother and cleaner.
 Good stuff!  
  
[\*] In fairness, since the last post we did some major renovations on
the house, sold it, moved to [Graeagle
CA](https://en.wikipedia.org/wiki/Graeagle), I got a new job, and we
bought a house in Portland.  Last step will be moving up there.  Maybe
once we settle in, I'll find time to write stuff here now and then!

