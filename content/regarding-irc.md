Title: Regarding IRC
Date: 2015-08-17 19:26
Author: Christopher Aedo
Tags: 
Slug: regarding-irc

All the work I do with [OpenStack](http://www.openstack.org) is
coordinated through mailing lists, IRC and design summits every
six months. For quite a while I handled IRC by just keeping a
screen up running [Irssi](http://www.irssi.org/) on a VM on some
cloud or another. That approach was quick and easy, and kept my
identity online at all times. I could get connected from
anywhere, and scroll-back to see what I had missed.

That left a lot to be desired though. It meant I had
no easy way to get connected from my phone or basically anything
other than via SSH (from a machine that had a private key I could
use to connect). It also meant getting notified when someone
wanted my attention on IRC was not easy. It was possible in a
limited fashion anyway - I just logged everything in Irssi and
then used tail, grep and notify-message like this: "tail -n 1 -q
-f ~/irclog/*/*.log|grep -i --line-buffered docaedo"|while read
line;do notify-send "IRC Message" "${line}";done

I stuck with this method almost entirely because it was quick
and easy and worked well enough for the last year or so.
In the last few months though keeping connected with folks via
IRC has become a lot more important to me, which meant I needed
to step it up a little bit and find a better solution.

Most of the people I know use an IRC bouncer, and the popular
choice is [ZNC](http://wiki.znc.in/ZNC). That IRC proxy
maintains your presence online, and makes it easy to connect from
different devices, with different IRC clients.  Sean Dague wrote
a really good doc on [his IRC proxy setup](https://dague.net/2014/09/13/my-irc-proxy-setup/).
If you are planning to take that route, you would have a hard
time finding a better guide (he even includes a puppet manifest
to automate the setup!)

He also mentions [IRCCloud](https://www.irccloud.com/) as an
alternative approach.  For $5/month, they provide a web interface
to IRC along with an app for iOS or Android. The app is pretty
decent, the web UI is easy to understand and use, and if you are
lazy, it's about the quickest and easiest way to get up and
running. No VM required, nothing to maintain. Just create an
account, sign in and you're good to go. I gave it a try for a
bit and was impressed. Someone who has been using it for
a while though warned there are connectivity problems that crop
up (DDoS's and such), and it's ugly when they do.

That same conversation (on IRC of course!) led me to
[WeeChat](https://weechat.org/), which embarrassingly I had never
heard of. (I feel like that happens too often even though I
seriously do my best to stay on top of all things nerdlife!) This
turned out to be the perfect solution for staying connected on
IRC from multiple clients easily.

On my Ubuntu VM getting up and running was as easy as "apt-get
install weechat".  Moments later, I was back online with a very
powerful shell client. One or two commands later I had enabled
the relay and connected the [free Android
client](https://play.google.com/store/apps/details?id=com.ubergeek42.WeechatAndroid)
from [my phone](https://republicwireless.com). It was working
beautifully, and alerting me for any messages mentioning me (or
anything else that I specified as watch-words). Next up I tried
[Glowing Bear](http://www.glowing-bear.org/), a browser-based
front-end for WeeChat and found it looked great, was easy to use,
and gave me one more easy way to get connected to my IRC
identity. The [Glowing Bear
source](https://github.com/glowing-bear/glowing-bear) is open and
available, and you can run it locally as a Firefox or Chrome app, hosted on
github pages, or served by your own web server.

After playing around for a few minutes, I set up a cert so I
could connect to my WeeChat relay over SSL following [these
quick and easy
instructions](https://4z2.de/2014/07/06/weechat-trusted-relay).

So in short, if you have any reason to use IRC with any
regularity I honestly think [WeeChat](https://weechat.org/) is
the best option available assuming you have access to an
always-on VM and just a tiny bit of technical ability.

If you're on IRC by the way, look me up and say hi! (I'm docaedo on
[Freenode](http://freenode.net)).

