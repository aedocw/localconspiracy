Title: Week 1 - 3 for 3
Date: 2010-04-11 04:10
Author: Christopher
Tags: 
Slug: week-1-3-for-3

![elevation profile](http://3.bp.blogspot.com/_culjsMKCHb4/S8FEdM-7M7I/AAAAAAAAWjQ/TrcKdHzurSI/s1600/elevation+profile.png)  
This was the first week to put up or shut up - I committed myself to
riding my bike to work at least three days a week in April. It was
raining cats and dogs Monday morning and I'd only gotten half a nights
sleep due to a vomiting toddler, so I talked myself into driving instead
of riding. That meant I was going to be riding two days in a row - I was
a little scared...  
  
All in all, it was not half bad. My ride time improved bit by bit
(except I took it easy on myself on Friday but was still only like two
minutes slower than Thursday!)  
  
With Heather's help I also found my Garmin ANT stick, so if you want to
see the gory details the links are below. It was a pretty fun week of
riding, and I'm looking forward to riding my bike to work a whole lot
more this year. (By the way the bike path along the Orange Line is
fantastic!)  
  
Anyone else riding their bike to work? What's it like for you?  

* [To Work (4/6)](http://connect.garmin.com/activity/29236839)  
* [Back Home (4/6)](http://connect.garmin.com/activity/29236841)  
* [To Work (4/8)](http://connect.garmin.com/activity/29425497)  
* [Back Home (4/8)](http://connect.garmin.com/activity/29425500)  
* [To Work (4/9)](http://connect.garmin.com/activity/29509355)  
* [Back Home (4/9)](http://connect.garmin.com/activity/29509364)  

