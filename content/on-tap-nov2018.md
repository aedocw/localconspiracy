Title: Whats On Tap, November 2018
Date: 2018-11-18 13:00
Author: Christopher Aedo
Tags: beer, kitchentaps
Slug: on-tap-nov2018

![image](/images/on-tap-nov2018.jpg "four beer taps")

Sticking with my promise to update the blog when I rotate what's
on tap, here comes November's entry.

The last round of beer lasted pretty long.  That's due to only
having one bbq party, and me drinking less beer these last few
months.

First up is another pale ale. Basically same recipe as last time
except no clarifying agents (usually I throw a whirlfloc tab in),
and I threw the dry hops in while the yeast was at high krausen.
As expected it came out very hazy (NE IPA style), and has really
good hop aroma.  I love this one!

The porter is a toasty malty wonderful beer to have on tap for
the winter season. It could handle aging a while, but it's pretty
good fresh out of the fermenter.

Finally I've got the holiday ale, your classic winter warmer. Not
much to say about this one other than I expect it to be a big
help getting me through the rainy winter months ahead.

Here's the recipe:

~~~~
9 lbs 8.0 oz          Pale Malt (2 Row) US (2.0 SRM)           Grain         1        76.0 %        
1 lbs 8.0 oz          Caramel/Crystal Malt -120L (120.0 SRM)   Grain         2        12.0 %        
1 lbs                 Caramel/Crystal Malt - 80L (80.0 SRM)    Grain         3        8.0 %         
4.0 oz                Cara-Pils/Dextrine (2.0 SRM)             Grain         4        2.0 %         
4.0 oz                Roasted Barley (300.0 SRM)               Grain         5        2.0 %         
1.00 oz               Galena [12.50 %] - First Wort 60.0 min   Hop           6        46.2 IBUs     
0.50 oz               Cascade [5.50 %] - First Wort 60.0 min   Hop           7        10.2 IBUs     
0.50 oz               Willamette [5.50 %] - First Wort 60.0 mi Hop           8        10.2 IBUs     
1.00 oz               Tettnang [4.50 %] - Boil 30.0 min        Hop           9        11.6 IBUs     
0.50 oz               Goldings, East Kent [5.00 %] - Boil 0.0  Hop           10       0.0 IBUs      
1.50 oz               Goldings, East Kent [5.00 %] - Dry Hop 3 Hop           12       0.0 IBUs      
1.00 oz               Cascade [5.50 %] - Dry Hop 3.0 Days      Hop           13       0.0 IBUs 
~~~~
