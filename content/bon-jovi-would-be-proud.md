Title: Bon Jovi would be proud
Date: 2010-04-04 20:00
Author: Christopher
Tags: 
Slug: bon-jovi-would-be-proud

I know people think it was about something else, but Bon Jovi's "Wanted
Dead Or Alive" was all about bicycles. Steel horse? What else could he
have been talking about? That dude is seriously into bikes...  
  
So I'm ready to kick off my "ride three days a week in April" plan this
Monday. The "test ride" to work last Monday wasn't half as bad as I
feared. It took longer than I expected (1:12), but I'll get faster. This
weekend I got panniers so I'll have enough room for clothes and the
computer. I also swapped the pedals, as the bearings were starting to go
in one of the old ones. Light is charged up in case I'm caught in the
dark, and everything is good to go.  
  
I'm using a Garmin 405 to log the mileage, but haven't found the USB key
that transfers the data up to the garmin site... I think I know exactly
which bag it's in, only problem is I have no idea where that bag is.
I'll search for another week before I break down and buy a replacement
(but if you've got an extra, please send it my way!)  
  
Oh by the way, thanks LA weather for getting chilly and rainy for my
first day riding to work :(  
  
![picture](/images/Screen+shot+2010-04-04+at+10.59.56+AM.png)

