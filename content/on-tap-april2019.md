Title: Whats On Tap, April 2019
Date: 2019-04-29 21:15
Author: Christopher Aedo
Tags: beer, kitchentaps
Slug: on-tap-apr2019

![image](/images/on-tap-190429.jpg "four beer taps")

Quick update to the blog about what's on tap these days!

It's been quite a while since the last update.  That is mostly
because I've been drinking a less lately.  Busy, and watching my
calories pretty closely while I try to drop a few pounds.
Usually that means I don't have room in the intake budget for the
extra 200kcal or so.  Just the same, it seems criminal to not
have beer on tap!

There's still some of that excellent xmas beer left, but probably
only a few pints left - it will be gone soon but I will
definitely brew that one again when winter comes back around.

The beer on the far left is a little obscured, but it's a N.E.
IPA.  Sort of.  I was experimenting with Imperial's "Juicy"
yeast, and dumped a ridiculous amount of hops in during high
krausen.  At first I thought it turned out great, then a few
weeks later I changed my mind.  Then a month or two later, and
having had a few other NE IPAs, I reconsidered - this beer is
really not too bad.

Next is a batch of [Sierra Nevada
Resilience](https://sierranevada.com/beer/resilience-butte-county-proud-ipa/),
a beer they created as a fundraiser for rebuilding after the Camp
Fire.  They shared the recipe with brewers large and small, and
lots of other brewers made their own version.  I've never tried
one, but I'm going to see if I can find some to sample, and see
how close my version turned out.

Finally, my frankenpils - a non-traditional pilsner made with ale
yeast.  I used Imperial Dry Hop yeast for this batch so it would
serve as a yeast starter for the Resilience IPA (which I just
pitched right on top of the yeast cake after racking the
frankenpils).  This one turned out really good, even though it's
got a little too high of an ABV to be a reasonable pilsner.
Still very drinkable and tasty though!

