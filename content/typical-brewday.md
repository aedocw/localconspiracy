Title: A Typical Brewday
Date: 2016-12-30 12:00
Author: Christopher Aedo
Tags: homebrew, beer
Slug: typical-brewday

While writing about [my electric brewing
system](/2016/12/electric-brewery.html) I thought it would be helpful
to outline what a typical brew day looks like with that setup.  Since
that post was already getting too long I decided to put this in it's
own relatively small post.

The FIRST step is to figure out what I'm going to brew.  Since I've
moved up to 10gal batches and have the all electric system, I'm
brewing more than I can reasonably drink.  I guess it turns out I like
making beer even more than I like drinking it.  That means even when
I've got four kegs on tap, I'm still thinking about what to brew next.
I keep [all my recipes](https://www.brewtoad.com/users/4059/recipes)
on [Brewtoad](https://www.brewtoad.com), so when I finally have room
for another batch of beer I pick one of my favorite recipes and then
go shopping.  A few days before I plan to brew, I visit [my local
homebrew shop, Above The Rest](http://atr-homebreweast.com/) to pick
up the necessary ingredients so on brew day I've got everything I'm
going to need.

On brew day (or the night before) I go back to the recipe on BrewToad
and use the mash water calculator, choosing the mash profile for
"Single Infusion, Sparge Rinse (BIAB)".  Even though I'm not doing a
brew-in-a-bag brew, the water calculation works out really well for
my setup.  I fill the HLT to the top measurement mark and then
transfer the right amount for the strike water into the mash tun.
Then I top the HLT back up.  (If I want to get an early start on my
brewing, I'll do this the night before).  Once the water levels are
right I set one pump to circulate the water in the HLT, and the other
pump to circulate the water from the mash, through the coil, and back
to the top of the mash tun.

When the pumps are running, I go to the
[CraftBeerPi](http://craftbeerpi.com/) URL on the RaspberryPi and set
the HLT to hold at 140.  It usually takes an hour or so to hit this
temp so I've got plenty of time to go do other stuff without needing
to watch the system.  The mash tun lags behind by around five minutes,
so I wait for the mash to stabilize at 140 before mashing in.

![mash prep](/images/mash-prep.jpg)

Next I shut off the mash pump and start to stir in my grains.  I've
got a big mash paddle and go relatively slow to make sure they're
thoroughly wetted and there are no dough balls.  Once all the grains
are in I stir for another minute or two just for good measure.  Then I
make sure the mash return/sparge valve is open all the way and I turn
on the mash pump.  If I were less lazy I would vorlauf first, but the
pump seems to do a really good job of it.  When it's running clear, I
close the valve down so it's moving slower and not spraying the mash
too much.  I know I run the risk of hot-side aeration with this but I
really haven't noticed any flavor impact (then again I haven't gone
through [the BJCP](http://www.bjcp.org/index.php) yet so maybe there's
some off flavors slipping in that I don't notice?)

Once the mash is recirculating through the coil in the HLT, I set the
target temp to 151 (or higher if the style/recipe calls for it).  It
usually takes 20 minutes or so for the mash to hit 151, during which
time I keep a close eye on it to make sure it's still circulating nicely
and not stuck or anything.

When I reach the target temp I start a timer for 70 minutes, and go do
other stuff.  When the timer goes off I set the HLT temp to 173 and
reset the timer for 20 minutes.  This starts a slow-ish ramp up to
mash out temp while the grain is still recirculating so the grain bed
will reach mash out temp and the sparge water will be ready as well.
([CraftBeerPi](http://craftbeerpi.com/) does have the functionality for
setting multiple steps to automate a step-mash but I haven't gotten
that to work right, and since the only step is to mash out I haven't
tried to figure out what I'm doing wrong.)

Now I shut down both pumps and change the connections.  The mash-out
goes to the inlet on one pump with the outlet going to the boil
kettle.  The other pump was recirculating the water in the HLT, so I
move the output of that pump to the inlet on the coil.  The outlet of
the coil still goes to the lid of the mash tun as it was doing while
recirculating the mash.

![mash prep](/images/mash-out.jpg)

When the connections are ready I turn the pumps back on and start
pumping out the mash tun into the boil kettle while fly-sparging with
the water from the HLT.  I watch to make sure I transfer the right
amount for the sparge and shut that pump off (and close the valves)
when complete.  Meanwhile the mash is still transferring slowly into
the boil kettle.  As soon as the heating element in the boil kettle is
completely submerged, I turn on that element to get the wort up to
boil.

When the mash is drained I turn off that pump and close the valves.
At this point I keep a close eye on the boil kettle to watch for boil
overs.  Generally I just shut off the heating element when the hot
break begins (and sometimes skim off some of the foam to help it
along).  Depending on the grain bill I sometimes have to cycle the
heating element for a while until the foaming stops.  Then I put in my
bittering hops and start the timer for other additions.

While the brew is boiling, I remove the spent grains from the mash tun
and then clean it out.  When the brew is getting near the end I empty
any remaining hot water from the HLT and pack it full of ice and cold
water.  I also make sure my fermenter is sanitized and ready to go.

When the brew is complete, I shut off the heating element and connect
the boil kettle to the inlet of the mash pump.  The outlet then goes
to the coil, and the output of the coil goes to the lid I use for
sparging (which has [this fly sparge attachment from Northern
Brewer](http://www.northernbrewer.com/fermenters-favorites-fighter-jet-sparge) in it.)
Pumping the wort slowly through the coil in the HLT that's filled with
ice water has worked out great for chilling the wort quickly, and the
fly sparge fitting oxygenates the wort nicely.

![to fermenter](/images/to-fermenter.jpg)

After the wort has transferred to the fermenter, it's time to clean.
At this point the mash tun and HLT are already clean, but the inside
of the coil and the boil kettle both need to attention.  I first rinse
out most of the trub from the boil kettle with a hose.  Then I
transfer four gallons of the chiller water from the HLT into the
kettle and raise the temp to 140.  Recirculating that hot water
through the coil, I dump in 4oz of "powdered beer wash" aka PBW.  This
solution recirculates for 30 minutes, with the HLT holding the temp
steady.  When the time is almost up I use a brush to make sure the
heating element is clean and free of debris.  Then I pump out the PBW
solution (and dump whatever can't be pumped out of the boil kettle.)
Then I transfer the remaining water from the HLT into the boil kettle
and recirculate that for another 30 minutes.  When that's done I pump
it all out and I'm ready to brew again!

I haven't watched the time too closely on these brew days to be
honest because I have so much extra time during the process to do
other stuff.  I haven't had any issues with temperature overshoots
during the mash, and once things are running I can walk away until the
next step.  It's pretty convenient to check in on things on my phone
in fact!  BUT it also means I can't say for sure that this is a 5 hour
brew day, or more (or less).  Regardless, it's been pretty awesome for
consistency and convenience.  Switching to all electric is a decision
I'm really happy with, would definitely recommend it.

