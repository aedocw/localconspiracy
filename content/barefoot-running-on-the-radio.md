Title: Barefoot running on the radio
Date: 2010-03-18 03:35
Author: Christopher Aedo
Tags: 
Slug: barefoot-running-on-the-radio

On national radio, the editor-in-chief of Runner's World (David Willey)
shills for the shoe companies while spreading misinformation. This piece
on NPR from last week started promisingly, but the guy doing the story
didn't bother to question this statement:  
"If you are a very efficient and biomechanically gifted runner, running
barefoot could probably work for you."  
  
I was not surprised that the editor of a running magazine (which makes
nearly all of it's money from shoe ads) was dismissive of barefoot
running. I WAS surprised that the guy from NPR presented Willey as the
expert on all things running, and didn't question a single thing he
said.  
  
The worst quote, when asked what would happen if everyone chucked their
shoes, Willey replies "If a lot of runners - or all the runners out
there in America - did that tomorrow, the vast majority of them would
get hurt very quickly and would have to stop running for a very long
time."  
  
The audio and print story is online at:  
<http://www.npr.org/templates/story/story.php?storyId=112995970>  
  
-Christopher

