FROM python:2.7-buster
RUN apt-get -o Acquire::Check-Valid-Until=false update && \
    apt-get install certbot -y --no-install-recommends
