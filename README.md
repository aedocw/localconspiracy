This is the repository for the worlds most exciting website,
localconspiracy.com.  It gets 1.5 million visitors every time.
Historic numbers of visitors.  Most popular site on the
internet.

Generate local content with:
source .venv/bin/activate
pelican -s publishconf.py -o public -t themes/tuxlite_tbs content

Hosted on gitlab-pages and secured with LetsEncrypt cert:
https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/

MANUAL: Renew cert with "sudo certbot certonly -d localconspiracy.com --manual"

Automated:
https://www.harenslak.nl/blog/https-letsencrypt-gitlab-hugo/
https://gitlab.com/jwishnie/letsencrypt-renewal

Adding certbot image:
vi Dockerfile
docker build -t certbot_image .
docker images
docker build -t registry.gitlab.com/aedocw/localconspiracy/certbot_image .
docker push registry.gitlab.com/aedocw/localconspiracy/certbot_image
